import sys

spkutt = {}
if ( len(sys.argv) == 2):
    with open(sys.argv[1]) as f:
        content = f.readlines()
        for line in content:
            uttspk = line.split(" ")
            if (uttspk[1] in spkutt):
                spkutt[uttspk[1]].append(uttspk[0])
            else:
                spkutt[uttspk[1]]=[]
                spkutt[uttspk[1]].append(uttspk[0])
    for key in spkutt :
        str = key.strip()
        for utt in spkutt[key]:
            str += " "+utt.strip()
        print(str)


elif ( len(sys.argv) == 3):  
    with open(sys.argv[1]) as f:
        content = f.readlines()
        for line in content:
            uttspk = line.split(" ")
            speed = uttspk[0].split("-t")[-1]
            if (speed in spkutt):
                spkutt[speed].append(uttspk[0])
            else:
                spkutt[speed]=[]
                spkutt[speed].append(uttspk[0])

    if ( sys.argv[2] == "speed_spk2utt"):            
        for key in spkutt :
            str = key.strip()
            for utt in spkutt[key]:
                str += " "+utt.strip()
            print(str)

    elif ( sys.argv[2] == "speed_utt2spk"):
        for key in spkutt :
            for utt in spkutt[key]:
                print(utt.strip() + " " + key.strip())


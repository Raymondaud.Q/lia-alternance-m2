# Overview

* [ Preprocessing for MELD SER & SSR ](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/tree/master/QRKalTorch-Toolkit/preprocessing_meld_emotion)
* [ Preprocessing for Voxceleb Speaking Rate ](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/tree/master/QRKalTorch-Toolkit/preprocessing_voxceleb_tempo)
* [ Shell Launcher Script  ](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/tree/master/QRKalTorch-Toolkit/run_train_test.sh)
* [ PyTorch ML Pipeline ](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/tree/master/QRKalTorch-Toolkit/train_test.py)
* [ Emotion & Sentiment Results ](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/tree/master/QRKalTorch-Toolkit/exp)


# Dependencies

* Kaldi Pybind 11 : [Follow this Readme](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/tree/master/SpeechBrain-UrbanSound-Recipe/exp_scripts/kaldi_mfcc_recipe)	
* Install [PyTorch](https://pytorch.org/)


# Run

* First check and fix arg.data in **train_test.py's parse_args function** in order to match your dataset location.
* Check and fix **run_train_test.sh** paths. 

## Voxceleb - Preprocessings

* `cd preprocessing_voxceleb_tempo` and check paths in run.sh 
* `./run.sh voxceleb1`
* `./run.sh voxceleb2`
* No features provided in this rep : to large (40GB)

### Voxceleb - Startup

* `cd ../`
* `sbatch run_train_test.sh batch_size=128 num_epoch=5 num_iterations=75 lr=0.001 architecture=tdnn task=tempo` **or** `sbatch run_train_test.sh batch_size=128 num_epoch=5 num_iterations=75 lr=0.001 architecture=mlp task=tempo` ...


## MELD - Preprocessings

* You can use features provided in `preprocessing_meld_emotion/featuresMELD.zip` 
	
* `cd preprocessing_meld_emotion` and check paths in run.sh 
	* For **emotion task** (SER) arg should be **task=2** as follows
		* `./run.sh task=2`
	* For **sentiment task** (SSR) arg should be **task=3** as follows
		* `./run.sh task=3`

### MELD - Startup

* `cd ../`
* `sbatch run_train_test.sh batch_size=128 num_epoch=5 num_iterations=75 lr=0.001 architecture=tdnn task=emotion` **or** `sbatch run_train_test.sh batch_size=128 num_epoch=5 num_iterations=75 lr=0.001 architecture=mlp task=sentiment` ...


## Inference in KALDI TDNNF acoustic model 

In order to use infered MFCC as feature you can an specify acoustic model as argument

* Extract `acoustic_model.zip`
* Run previous recipe adding `acoustic_model=YourModel`
	* ex : `sbatch run_train_test.sh batch_size=128 num_epoch=5 num_iterations=75 lr=0.001 architecture=tdnn task=emotion` **or** `sbatch run_train_test.sh batch_size=128 num_epoch=5 num_iterations=75 lr=0.001 architecture=mlp task=sentiment acoustic_model=finalf10`


**/!\ - Notes - /!\\** 

* Multi-Layer Perceptron is available with the option `architecture=mlp` ; ECAPA-TDNN with `architecture=tdnn`
* Speaking rate task (`task=tempo`) is made on Voxceleb while SER & SSR (`task=emotion` & `task=sentiment`) are made on MELD dataset
import os, sys, glob, argparse, asyncio,time, math, torch, gc, random
import numpy as np
import copy
import torch.nn as nn
import torch.nn.functional as F
from joblib import Parallel, delayed
from torch.autograd import Variable
from torch.utils.data import Dataset
from sklearn.metrics import f1_score, confusion_matrix
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from collections import OrderedDict, Counter
from ignite.metrics import Accuracy

def load_n_col(file,numpy=False):
    data = []
    with open(file) as fp:
        for line in fp:
            data.append(line.strip().split(' '))
    columns = list(zip(*data))
    if numpy:
        columns = [np.array(list(i)) for i in columns]
    else:
        columns = [list(i) for i in columns]
    return columns

def odict_from_2_col(file, numpy=False):
    col0, col1 = load_n_col(file, numpy=numpy)
    return OrderedDict({c0: c1 for c0, c1 in zip(col0, col1)})

def load_one_tomany(file):
    one = []
    many = []
    with open(file) as fp:
        for line in fp:
            line = line.strip().split(' ', 1)
            one.append(line[0])
            m = line[1]
            many.append(m)
    return one, many

def train_transform(feats, seqlen):
    leeway = feats.shape[0] - seqlen
    startslice = np.random.randint(0, int(leeway)) if leeway > 0  else 0
    feats = feats[startslice:startslice+seqlen] if leeway > 0 else np.pad(feats, [(0,-leeway), (0,0)], 'constant')
    return torch.FloatTensor(feats)

async def get_item_train(instructions):
    fpath = instructions[0]
    seqlen = instructions[1]
    raw_feats = read_mat(fpath)
    feats = train_transform(raw_feats, seqlen)
    return feats

async def get_item_test(filepath):
    raw_feats = read_mat(filepath)
    return torch.FloatTensor(raw_feats)

def async_map(coroutine_func, iterable):
    loop = asyncio.get_event_loop()
    future = asyncio.gather(*(coroutine_func(param) for param in iterable))
    return loop.run_until_complete(future)

class Logger():

    # Composite Logger that allows to print to multiple files and to cli in a single call
    # - Made by Quentin Raymondaud
    # - https://gitlab.com/Raymondaud.Q/

    def __init__(self, path_list, mode="a", embedded_loggers=[]):

        self.embedded_loggers = embedded_loggers
        self.log_files=[]
        for path in path_list : 
            self.log_files.append(open(path, mode))

    def log(self, content="Default_content", cli=True, index_exclude=[]):

        if cli == True:
            print(content)

        for logger in self.embedded_loggers:
            logger.log(content=content, cli=False, index_exclude=index_exclude)
        
        cnt = 0
        for file in self.log_files:
            if not cnt in index_exclude :
                print(content, file=file)
            cnt+=1

class F1Loss(nn.Module):
    """ F1Loss
        Created by: Zhang Shuai
        Email: shuaizzz666@gmail.com
        f1_loss = 2*r*p / (p + r)
    Args:
        weight: An array of shape [C,]
        predict: A float32 tensor of shape [N, C, *], for Semantic segmentation task is [N, C, H, W]
        target: A int64 tensor of shape [N, *], for Semantic segmentation task is [N, H, W]
    Return:
        diceloss
    """
    def __init__(self, weight=None):
        super(F1Loss, self).__init__()
        if weight is not None:
            weight = torch.Tensor(weight)
            self.weight = weight / torch.sum(weight) # Normalized weight
        self.smooth = 1e-5

    def forward(self, input, target):
        N, C = input.size()[:2]
        _, predict = torch.max(input, 1)# # (N, C, *) ==> (N, 1, *)

        predict = predict.view(N, 1, -1) # (N, 1, *)
        target = target.view(N, 1, -1) # (N, 1, *)
        last_size = target.size(-1)

        ## convert predict & target (N, 1, *) into one hot vector (N, C, *)
        predict_onehot = torch.zeros((N, C, last_size)).cuda() # (N, 1, *) ==> (N, C, *)
        predict_onehot.scatter_(1, predict, 1) # (N, C, *)
        target_onehot = torch.zeros((N, C, last_size)).cuda() # (N, 1, *) ==> (N, C, *)
        target_onehot.scatter_(1, target, 1) # (N, C, *)

        true_positive = torch.sum(predict_onehot * target_onehot, dim=2)  # (N, C)
        total_target = torch.sum(target_onehot, dim=2)  # (N, C)
        total_predict = torch.sum(predict_onehot, dim=2)  # (N, C)
        ## Recall = TP / (TP + FN), precision = TP / (TP + TP)
        recall = (true_positive + self.smooth) / (total_target + self.smooth)  # (N, C)
        precision = (true_positive + self.smooth) / (total_predict + self.smooth)  # (N, C)
        f1 = 2 * recall * precision / (recall + precision)

        if hasattr(self, 'weight'):
            if self.weight.type() != input.type():
                self.weight = self.weight.type_as(input.data)
                f1 = f1 * self.weight * C  # (N, C)
        f1_loss = 1 - torch.mean(f1)  # 1

        return f1_loss

class MLP(nn.Module):

    def __init__(self, args):

        super(MLP, self).__init__()
        self.utt_split_size = args.max_seq_len
        self.mlp = nn.Sequential(
            nn.Linear(args.feature_size * self.utt_split_size , 512),
            nn.Linear(512, args.output_size),
            #nn.Softmax(dim=-1)
        )

    def forward(self, x):
        #print(x.shape)
        x = torch.flatten(x, start_dim=1)
        #print(x.shape)
        out = self.mlp(x)
        return out

class DatasetManager(Dataset):

    def __init__(self, test_index=0, data_base_path="Provide_A_Valid_Path/PLZ", asynchr=False, num_workers=4):

        self.num_workers = num_workers
        self.test_index = test_index
        utt2spk_path = os.path.join(data_base_path, 'utt2spk')
        spk2utt_path = os.path.join(data_base_path, 'spk2utt')
        feats_scp_path = os.path.join(data_base_path, 'feats.scp')

        assert os.path.isfile(utt2spk_path)
        assert os.path.isfile(feats_scp_path)
        assert os.path.isfile(spk2utt_path)

        self.utts, self.uspkrs = load_n_col(utt2spk_path)
        self.utt_fpath_dict = odict_from_2_col(feats_scp_path)
        self.spkrs, self.spkutts = load_one_tomany(spk2utt_path)
        self.label_enc = LabelEncoder()
        self.label2_enc = OneHotEncoder(handle_unknown='ignore')
        self.spkrs = self.label_enc.fit_transform(self.spkrs)
        self.label2_enc.fit(self.spkrs.reshape(-1,1))
        self.uspkrs = self.label_enc.transform(self.uspkrs)

        print("sentiment", self.spkrs)
        print("uspkrs ", self.uspkrs)
        
        self.spk_utt_dict = OrderedDict({k:v for k,v in zip(self.spkrs, self.spkutts)})
        #self.utt_spkr_dict = OrderedDict({k:v for k,v in zip(self.utts, self.uspkrs)})

        self.first_batch = True
        self.utt_list = list(self.utt_fpath_dict.keys())
        self.num_classes = len(self.label_enc.classes_)
        self.asynchr = asynchr
        self.allowed_classes = copy.deepcopy(self.spkrs) # classes the data can be drawn from
        self.idpool = copy.deepcopy(self.allowed_classes)
        self.ignored = []

    def __len__(self):
        return len(self.utt_list)

    @staticmethod
    # Ici on utilise, s'il existe, notre modèle acoustique kaldi pour transformer nos features
    # Dont le chemin vers le fichier est passé dans l'argument --acoustic_model et désérialisé dans le main dans la variable nnet
    def get_item(instructions): 

        fpath = instructions[0]
        seqlen = instructions[1]
        feats = kaldi.read_mat(fpath)

        if ( nnet != 0 ):
            feats = np.matrix(nnet.GetOutput(feats))
        else:
            feats = feats.numpy()

        feats = train_transform(feats, seqlen)

        return feats

    def set_remaining_classes(self, remaining:list):

        self.allowed_classes = sorted(list(set(remaining)))
        self.ignored = sorted(set(np.arange(self.num_classes)) - set(remaining))
        self.idpool = copy.deepcopy(self.allowed_classes)

    def set_ignored_classes(self, ignored:list):

        self.ignored = sorted(list(set(ignored)))
        self.allowed_classes = sorted(set(np.arange(self.num_classes)) - set(ignored))
        self.idpool = copy.deepcopy(self.allowed_classes)

    def set_remaining_classes_comb(self, remaining:list, combined_class_label):

        remaining.append(combined_class_label)
        self.allowed_classes = sorted(list(set(remaining)))
        self.ignored = sorted(set(np.arange(self.num_classes)) - set(remaining))
        self.idpool = copy.deepcopy(self.allowed_classes)        
        for ig in self.ignored:
            # modify self.spk_utt_dict[combined_class_label] to contain all the ignored ids utterances
            self.spk_utt_dict[combined_class_label] += self.spk_utt_dict[ig]
        self.spk_utt_dict[combined_class_label] = list(set(self.spk_utt_dict[combined_class_label]))

    # New version after COLING publication
    def get_batches_meld(self, batch_size=None, max_seq_len=400, nb_important_speaker=5, max_utt_per_important_speaker=None, max_utt_per_label=None):

        # with Parallel(n_jobs=self.num_workers) as parallel:
        # Metric learning assumption large num classes

        batch_size = len(self.utts) if (batch_size == None ) else batch_size

        if max_utt_per_label == None:
            max_utt_per_label = int((batch_size/2)/len(self.allowed_classes))
            while max_utt_per_label * len(self.allowed_classes) < batch_size/2 :
                max_utt_per_label += 1

        max_utt_per_important_speaker = int((batch_size/2)/nb_important_speaker)

        lens = [ max_seq_len for _ in range(batch_size) ]
        
        available_utts = [ utt for utt in self.utt_fpath_dict ]
        utts_by_spk = {}
        
        for utt in available_utts:

            sutt = utt.split("_")
            label, spk = sutt[0], sutt[1]

            if spk not in utts_by_spk:

                utts_by_spk[spk] = {}
                utts_by_spk[spk][label] = { "nb":1, "paths":[self.utt_fpath_dict[utt]] }
                utts_by_spk[spk]["nb"] = 1

            else:

                if label not in utts_by_spk[spk]:
                    utts_by_spk[spk][label] = { "nb":1, "paths":[self.utt_fpath_dict[utt]] }
                    utts_by_spk[spk]["nb"] += 1

                else : 
                    utts_by_spk[spk][label]["paths"].append(self.utt_fpath_dict[utt])
                    utts_by_spk[spk]["nb"] += 1
                    utts_by_spk[spk][label]["nb"]+=1

        #for spk in utts_by_spk:
        #    print(f"Speaker %s has %i emotions : " % (spk,len(utts_by_spk[spk])))
        #    for label in utts_by_spk[spk]:
        #        print(f"\t\t Label : %s - Nb : %i " % (label, utts_by_spk[spk][label]['nb']))

        important_speaker_dict = {}

        for i in range(nb_important_speaker):
            tmpMax = 0 
            spkMax = ""

            for spk in utts_by_spk:
                if utts_by_spk[spk]["nb"] > tmpMax and spk not in important_speaker_dict:
                    tmpMax = utts_by_spk[spk]["nb"]
                    spkMax = spk

            important_speaker_dict[spkMax] = tmpMax

        logger.log(content=str(nb_important_speaker) + " important speakers are " + str(important_speaker_dict.keys()))

        cpy_utts = copy.deepcopy(utts_by_spk)
        keys = [ k for k in cpy_utts.keys() ]

        cnt_utt_per_label = {}
        r_label=self.label_enc.inverse_transform(self.allowed_classes)
        for i in r_label:
            cnt_utt_per_label[i] = 0

        cnt_utt_per_spk = {}
        for i in utts_by_spk:
            cnt_utt_per_spk[i] = 0

        batch_fpaths = []
        batch_ids = []
        nb_utt = len(available_utts)

        while True:

            for i in range(2):
                
                if (i == 0):
                    print("Important Speakers batching")
                    keys = [ k for  k in important_speaker_dict.keys() ]
                else : 
                    print("Diverse Speaker batching")
                    keyss = [ k for k in cpy_utts.keys() ]
                    for k in keyss :
                        if ( k in keys ):
                            keyss.remove(k)
                    keys = keyss
                    random.shuffle(keys)

                for spk in keys:
                    #print(len(batch_fpaths), batch_size, len(batch_fpaths) >= batch_size)
                    if len(batch_fpaths) >= batch_size:
                        break

                    for label in cpy_utts[spk]:
                        if ( label != "nb" ):
                            breaked = False
                            ids_to_remove = []
                            #print(cpy_utts[spk][label])
                            for i in range(cpy_utts[spk][label]['nb']) :

                                utt_label_limit = max_utt_per_label <= cnt_utt_per_label[label]

                                if ( spk in important_speaker_dict ): # Limiting utts for important speakers
                                    utt_spk_limit = max_utt_per_important_speaker <= cnt_utt_per_spk[spk]
                                else: # If not important speaker there is no limit (ie impossible to have much utt)
                                    utt_spk_limit = False

                                if utt_spk_limit or len(batch_fpaths) >= batch_size : 

                                    breaked = True
                                    break

                                elif ( not utt_label_limit and not utt_spk_limit or not spk in important_speaker_dict ) :
                                    if ( spk in important_speaker_dict):
                                        cnt_utt_per_spk[spk] += 1
                                        cnt_utt_per_label[label] += 1
                                    batch_fpaths.append(cpy_utts[spk][label]["paths"][i])
                                    batch_ids.append(label)
                                    ids_to_remove.append(i)

                                else :
                                    break

                            shift_removal = 0
                            for i in ids_to_remove : 
                                del cpy_utts[spk][label]["paths"][i+shift_removal]
                                cpy_utts[spk][label]["nb"] -= 1 
                                shift_removal -= 1
                                nb_utt -= 1

                            if breaked :
                                break

            logger.log(content="Utt number per label for important speakers: " + str(cnt_utt_per_label))
            #logger.log(content="Cnt utt per important spk: " + str(cnt_utt_per_spk))
            logger.log(content="Batch len: " + str(len(batch_ids)))
            cnt_utt_per_spk = {}
            cnt_utt_per_label = {}

            r_label=self.label_enc.inverse_transform(self.allowed_classes)

            for i in r_label:
                cnt_utt_per_label[i] = 0

            for i in utts_by_spk:
                cnt_utt_per_spk[i] = 0

            if len(batch_fpaths) >= batch_size:

                if self.asynchr:
                    batch_feats = async_map(get_item_train, zip(batch_fpaths, lens))
                else:
                    batch_feats = [self.get_item(a) for a in zip(batch_fpaths, lens)]
                    #batch_feats = Parallel(n_jobs=self.num_workers)(delayed(self.get_item)(a) for a in zip(batch_fpaths, lens))

                logger.log(content="INFO - %i batched utterance, %i left in train set" % (len(batch_fpaths), nb_utt))

                yield torch.stack(batch_feats), self.label_enc.transform(list(batch_ids))

            else :
                cpy_utts = copy.deepcopy(utts_by_spk)
                nb_utt = len(available_utts)

            batch_fpaths = []
            batch_ids = []


    # Version used in COLING publication
    def get_batches(self, batch_size=None, max_seq_len=400):

        # with Parallel(n_jobs=self.num_workers) as parallel:
        # Metric learning assumption large num classes
        batch_size = len(self.utts) if (batch_size == None ) else batch_size
        lens = [max_seq_len for _ in range(batch_size)]
        while True:
            if len(self.idpool) <= batch_size:
                batch_ids = np.array(self.idpool)
                self.idpool = copy.deepcopy(self.allowed_classes)
                rem_ids = np.random.choice(self.idpool, size=batch_size-len(batch_ids))#, replace=False)
                batch_ids = np.concatenate([batch_ids, rem_ids])
                self.idpool = list(set(self.idpool) - set(rem_ids))
            else:
                batch_ids = np.random.choice(self.idpool, size=batch_size)#, replace=False)
                self.idpool = list(set(self.idpool) - set(batch_ids))

            batch_fpaths = []
            for i in batch_ids:
                utt = np.random.choice(self.spk_utt_dict[i].split(" "))
                batch_fpaths.append(self.utt_fpath_dict[utt])

            if self.asynchr:
                batch_feats = async_map(get_item_train, zip(batch_fpaths, lens))
            else:
                batch_feats = [self.get_item(a) for a in zip(batch_fpaths, lens)]
                #batch_feats = Parallel(n_jobs=self.num_workers)(delayed(self.get_item)(a) for a in zip(batch_fpaths, lens))
            yield torch.stack(batch_feats), list(batch_ids)

    def get_test_utt(self):

        overflow=False
        if self.test_index+1 >= len(self.utts):
            print("/!\\ -- Back to Utt 0 -- /!\\")
            self.test_index = 0
            overflow = True

        end_index = 0
        if not overflow:
            end_index = self.test_index+1

        self.test_index = end_index
        feat_id = self.uspkrs[end_index]
        fpath = list(self.utt_fpath_dict.values())[end_index]
        raw_feat=kaldi.read_mat(fpath)
        
        if ( nnet != 0 ) :
            feat = torch.FloatTensor(nnet.GetOutput(raw_feat).numpy())
        else :
            feat = torch.FloatTensor(raw_feat.numpy())

        #print(feat, feat_id)
        return feat, feat_id



class TDNNBlock(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size=1, stride=1, padding=0, dilation=1, groups=1, bias=True):
        super(TDNNBlock, self).__init__()
        self.conv = nn.Conv1d(in_channels, out_channels, kernel_size=kernel_size, stride=stride, padding=padding, dilation=dilation, groups=groups, bias=bias)
        self.norm = nn.BatchNorm1d(out_channels)

    def forward(self, x):
        return self.norm(F.relu(self.conv(x)))

class Res2NetBlock(torch.nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size=1, stride=1, padding=0, dilation=1, bias=True, scale=4, groups=1):
        super(Res2NetBlock, self).__init__()
        self.scale = scale
        self.in_channel = in_channels // scale
        self.hidden_channel = out_channels // scale
        self.nums = scale if scale == 1 else scale - 1
        self.convs = []
        self.bns = []
        num_pad = math.floor(kernel_size/2)*dilation
        for i in range(self.nums):
            #self.convs.append(nn.Conv1d(self.in_channel, self.hidden_channel, kernel_size=kernel_size, stride=stride, padding=num_pad, dilation=dilation, bias=bias))
            self.convs.append(nn.Conv1d(self.in_channel, self.hidden_channel, kernel_size=kernel_size, stride=stride, padding=padding, dilation=dilation, bias=bias))
            self.bns.append(nn.BatchNorm1d(self.hidden_channel))
        self.convs = nn.ModuleList(self.convs)
        self.bns = nn.ModuleList(self.bns)

    def forward(self, x):
        out = []
        spx = torch.split(x, self.in_channel, 1)
        for i in range(self.nums):
            if i == 0:
                sp = spx[i]
            else:
                sp = sp + spx[i]
            sp = self.convs[i](sp)
            sp = self.bns[i](F.relu(sp))
            out.append(sp)
        if self.scale != 1:
            out.append(spx[self.nums])
        out = torch.cat(out, dim=1)
        return out

    '''
            y = []
            for i, x_i in enumerate(torch.chunk(x, self.scale, dim=1)):
                if i == 0:
                    y_i = x_i
                elif i == 1:
                    y_i = self.bns[i - 1](F.relu(self.convs[i - 1](x_i)))
                else:
                    y_i = self.bns[i - 1](F.relu(self.convs[i - 1](x_i + y_i)))
                y.append(y_i)
            y = torch.cat(y, dim=1)
            return y
    '''

class SEBlock(nn.Module):
    def __init__(self, in_channels, se_channels, out_channels):
        super(SEBlock, self).__init__()
        self.linear1 = nn.Linear(in_channels, se_channels)
        self.linear2 = nn.Linear(se_channels, out_channels)

    def forward(self, x):
        out = x.mean(dim=2)
        out = F.relu(self.linear1(out))
        out = torch.sigmoid(self.linear2(out))
        out = x * out.unsqueeze(2)
        return out

class SERes2NetBlock(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size=1, stride=1, padding=0, dilation=1, scale=8, se_channels=128, groups=1):
        super(SERes2NetBlock, self).__init__()

        self.tdnnblock1 = TDNNBlock(in_channels, out_channels, kernel_size=1, stride=1, padding=0)
        self.res2netblock = Res2NetBlock(in_channels, out_channels, kernel_size=kernel_size, stride=stride, padding=padding, dilation=dilation, scale=scale)
        self.tdnnblock2 = TDNNBlock(in_channels, out_channels, kernel_size=1, stride=1, padding=0)
        self.se_block = SEBlock(in_channels, se_channels, out_channels)
        self.shortcut = None
        if in_channels != out_channels:
            self.shortcut = Conv1d(in_channels, out_channels, kernel_size=1)

    def forward(self, x):
        residual = x
        if self.shortcut:
            residual = self.shortcut(x)
        x = self.tdnnblock1(x)
        x = self.res2netblock(x)
        x = self.tdnnblock2(x)
        x = self.se_block(x)

        return x + residual

class ECAPA_TDNN(torch.nn.Module):
    
    def __init__(self, args, channels=[1024, 1024, 1024, 1024, 3072], kernel_sizes=[6, 3, 3, 3], dilations=[ 1, 1, 1], groups=[1, 1, 1, 1, 1], attention_channels=256, res2net_scale=16, global_context=True):
        super(ECAPA_TDNN, self).__init__()
        self.layer1 = TDNNBlock(args.feature_size, channels[0], kernel_size=kernel_sizes[0], padding=2, groups=groups[0])
        self.layer2 = SERes2NetBlock(channels[0], channels[1], kernel_size=kernel_sizes[1], stride=1, padding=1, dilation=dilations[0], scale=res2net_scale, se_channels=attention_channels, groups=groups[1])
        self.layer3 = SERes2NetBlock(channels[1], channels[2], kernel_size=kernel_sizes[2], stride=1, padding=1, dilation=dilations[1], scale=res2net_scale, se_channels=attention_channels, groups=groups[2])
        self.layer4 = SERes2NetBlock(channels[2], channels[3], kernel_size=kernel_sizes[3], stride=1, padding=1, dilation=dilations[2], scale=res2net_scale, se_channels=attention_channels, groups=groups[3])
        self.conv = nn.Conv1d(channels[-1], args.feature_size, kernel_size=1)
        self.attention = nn.Sequential(
            nn.Conv1d(args.feature_size, 128, kernel_size=1),
            nn.BatchNorm1d(128),
            nn.Tanh(),
            nn.Conv1d(128, args.feature_size, kernel_size=1),
            nn.Softmax(dim=2),
        )
        self.norm_stats = torch.nn.BatchNorm1d(2*args.feature_size)
        self.fc_embed = nn.Linear(2*args.feature_size, args.output_size)
        for m in self.modules():
            if isinstance(m, nn.Conv1d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
            elif isinstance(m, nn.BatchNorm1d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)
        #self.fc_out = nn.Softmax(dim=-1) # Comment when CrossEntropyLoss

    def forward(self, x):
        x = x.transpose(1, 2)
        out1 = self.layer1(x)
        out2 = self.layer2(out1)
        out3 = self.layer3(out2)
        out4 = self.layer4(out3)
        out = torch.cat([out2, out3, out4], dim=1)
        out = F.relu(self.conv(out))
        w = self.attention(out)
        mu = torch.sum(out * w, dim=2)
        sg = torch.sqrt( ( torch.sum((out**2) * w, dim=2) - mu**2 ).clamp(min=1e-5) )
        out = torch.cat([mu,sg], dim=1)
        out = self.norm_stats(out)
        out = self.fc_embed(out)
        #out = self.fc_out(out) # Comment when CrossEntropyLoss
        #print(out.shape)
        return out

def init_model(args, modelName=None, load=False):



    if ( modelName != None ) :
        args.load_model = modelName

    if ( args.load_model != "") :

        model = torch.load(args.model_dir+"/"+feature_type+"_"+args.architecture+"_"+args.task+"_"+args.load_model+".mat")
        logger.log("> Model " + args.model_dir+"/"+feature_type+"_"+args.architecture+"_"+args.task+"_"+args.load_model+".mat" + " loaded")

    else : 

        if args.architecture == "tdnn" :
            model = ECAPA_TDNN(args) 
        elif args.architecture == "mlp":
            model = MLP(args)

    return model

def train_test(args):

    device = torch.device("cuda")
    use_cuda = not args.use_cuda and torch.cuda.is_available()

    print('='*30)
    print('USE_CUDA SET TO: {}'.format(use_cuda))
    print('CUDA AVAILABLE?: {}'.format(torch.cuda.is_available()))
    print('='*30)

    model = init_model(args)

    logger.log(content="ADAMW")

    #optimizer = torch.optim.Adam([{'params': model.parameters(), 'lr':args.lr, 'weight_decay':args.weight_decay}])
    #optimizer = torch.optim.SGD(model.parameters(), lr=args.lr, momentum=0.01)
    #optimizer = torch.optim.Adagrad(model.parameters(), lr=args.lr) # Works Ok
    #optimizer = torch.optim.RMSprop(model.parameters(), lr=args.lr)
    optimizer = torch.optim.AdamW([{'params': model.parameters(), 'lr':args.lr, 'weight_decay':args.weight_decay}])

    #criterion = nn.MSELoss() 
    criterion = nn.CrossEntropyLoss()
    #criterion = nn.NLLLoss()
    scheduler = torch.optim.lr_scheduler.CyclicLR(optimizer, args.lr, args.lr/50, step_size_up = args.num_iterations, step_size_down = args.num_iterations/2, cycle_momentum=False)
    #scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=30, gamma=0.5)
    running_loss = [np.nan for _ in range(500)]
    modelName = "pid-"+str(os.getpid())

    model = nn.DataParallel(model)
    model.to(f'cuda:{model.device_ids[0]}')

    if model != None :

        num_iter = 0
        bestModel = None
        bestScore = None
        num_bad_steps = 0

        train_utt = "/TRAIN_WAV"
        test_utt = "/TEST_WAV"
        dev_utt = "/DEV_WAV"

        if args.task == "tempo":

            train_utt = "voxceleb2"
            test_utt = "voxceleb1"
            dev_utt = "voxceleb1"

        ds_train = DatasetManager(data_base_path=args.data+train_utt)

        data_generator = 0

        if args.task != "tempo":
            data_generator = ds_train.get_batches_meld(batch_size=args.batch_size, max_seq_len=args.max_seq_len)
        else:
            data_generator = ds_train.get_batches(batch_size=args.batch_size, max_seq_len=args.max_seq_len)

        ds_test = DatasetManager(data_base_path=args.data+test_utt)
        ds_dev = DatasetManager(data_base_path=args.data+dev_utt)

        if args.mode != 'eval':

            for epochs in range(args.num_epochs):

                logger.log(content="\n-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-- Train epoch " + str(epochs) + " --=-=-=-=-=-=-=-=-=-=-=-=-=-\n")

                model.train()
                for iterations in range(args.num_iterations):

                    optimizer.step()

                    feats, iden = next(data_generator)
                    logger.log(content=f"shape of feats: {feats.shape}")
                    feats = feats.to(f'cuda:{model.device_ids[0]}')

                    iden = torch.LongTensor(iden).to(f'cuda:{model.device_ids[0]}') # FOR CrossEn & F1Loss
                    #iden = torch.FloatTensor(np.asarray(iden).reshape(-1,1)).to(f'cuda:{model.device_ids[0]}') # FOR MSELoss
                    preds = model(feats).to(f'cuda:{model.device_ids[0]}')

                    loss = criterion(preds, iden)
                    optimizer.zero_grad()
                    #loss.requires_grad = True # For F1 loss
                    #print("Values Pred --> ", preds)
                    #print("Values Iden --> ", iden)
                    #print("Loss ->", loss)
                    loss.backward()

                    running_loss.pop(0)
                    running_loss.append(loss.item())

                    rmean_loss = np.nanmean(np.array(running_loss))

                    num_iter = epochs*args.num_iterations+iterations

                    msg = "{}: {}: [{}/{}] \t C-Loss:{:.4f}, AvgLoss:{:.4f}, lr: {}, bs: {} ".format(args.model_dir, time.ctime(), iterations, args.num_iterations, loss.item(), rmean_loss, optimizer.param_groups[0]["lr"], len(feats))
                    # print("Iteration : " + str(num_iter) +" - "+ msg, file=train_log_file)
                    print("Iteration : " + str(num_iter) +" - "+ msg)
                    scheduler.step()

                    del feats
                    del iden
                    del preds
                    gc.collect()
                    torch.cuda.empty_cache()

                    #print(torch.cuda.memory_summary())
                    logger.log( content="\n-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-- Test iteration " + str(num_iter) + " on DEV -- LR :" + str(optimizer.param_groups[0]["lr"]) + " C-LOSS : " + str(loss.item()) + " AVG-LOSS :" + str(rmean_loss) + "\n")

                    model.eval()

                    labels = []
                    preds = []
                    
                    for i in range(0, len(ds_dev.utts)-1):

                        dev_feats, dev_iden = ds_dev.get_test_utt()
                        if (args.architecture =="mlp"):
                            dev_feats = train_transform(dev_feats, args.max_seq_len)
                        dev_feats = dev_feats.unsqueeze(0).to(f'cuda:{model.device_ids[0]}')
                        pred = model(dev_feats)
                        predcpu = pred.cpu().detach().numpy()
                        pred = torch.argmax(torch.LongTensor(predcpu), dim=1)
                        labels.append(int(dev_iden))
                        preds.append(int(pred)) 

                    print("Label, Pred : ", labels, preds)

                    cm = confusion_matrix(labels, preds)
                    fmeas = f1_score(labels, preds, average='weighted')

                    msg = "Confusion Matrix : \n" + str(cm) 
                    msg += "\nF-Measure : " + str(fmeas) 
                    logger.log(content=msg)

                    ## Best Model Save & RollBack Mechanism
                    if bestScore == None or bestScore < fmeas:
                        bestScore = fmeas
                        bestModel = modelName
                        print(args.model_dir,feature_type,args.architecture,args.task,modelName)
                        modelSave=args.model_dir+"/"+feature_type+"_"+args.architecture+"_"+args.task+"_"+modelName+".mat"
                        logger.log( content="> Save best model " + modelSave)
                        torch.save(model, modelSave)
                        num_bad_step = 0

                    if fmeas < bestScore : 
                        logger.log( content="> Bad Iter : Score ["+ str(fmeas) +"/"+ str(bestScore) + "] - N° badstep  [" + str(num_bad_step)+"/"+str(args.rollback_threshold) +"]")
                        num_bad_step += 1

                    if num_bad_step >= args.rollback_threshold :
                        logger.log( content="> Reload previous " + modelSave + " model after "+ str(num_bad_step) + " badstep")
                        model = init_model(args, modelName=modelName )
                        num_bad_step = 0

                    del pred
                    gc.collect()
                    torch.cuda.empty_cache()

            logger.log( content="\n-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-- Test Best Model " + bestModel + " on TEST --=-=-=-=-=-=-=-=-=-=-=-=-=-\n")

            del dev_feats
            del dev_iden
            gc.collect()
            torch.cuda.empty_cache()

            model = torch.load(args.model_dir+"/"+feature_type+"_"+args.architecture+"_"+args.task+"_"+bestModel+".mat")
            logger.log(content="> Best Model on DEV " + bestModel + " loaded")

        model.eval()
        labels = []
        preds = []
        for i in range(0, len(ds_test.utts)-1):

            test_feats, test_iden = ds_test.get_test_utt()
            if (args.architecture =="mlp"):
                test_feats = train_transform(test_feats, args.max_seq_len)
            test_feats = test_feats.unsqueeze(0).to(f'cuda:{model.device_ids[0]}')
            pred = model(test_feats)
            predcpu = pred.cpu().detach().numpy()
            pred = torch.argmax(torch.LongTensor(predcpu), dim=1)
            labels.append(int(test_iden))
            preds.append(int(pred)) 

        print("Label, Pred : ", labels, preds)

        cm = confusion_matrix(labels, preds)
        fmeas = f1_score(labels, preds, average='weighted')

        msg = "Confusion Matrix : \n" + str(cm) 
        msg += "\nF-Measure : " + str(fmeas) 
        logger.log(content=msg)

# Script Args Parser 
def parse_args():
    parser = argparse.ArgumentParser(description="Train model")
    parser.add_argument("--architecture", type=str, default="mlp") # mlp or tdnn
    parser.add_argument("--task", type=str, default="sentiment") # sentiment or emotion
    parser.add_argument("--embeddings", type=str , default="") # nothing, w2vCNN, w2vTRSFMR
    parser.add_argument("--kaldi_pybind_path", type=str, default="/Please_Provide --kaldi_pybind_path")
    parser.add_argument("--data", type=str, default="data/train/")
    parser.add_argument("--use_cuda", type=bool, default=True)
    parser.add_argument("--multi_gpu", type=bool, default=True)
    parser.add_argument("--batch_size", type=int, default=128)
    parser.add_argument("--max_seq_len", type=int, default=600) # avg utt length = 5.5 sec 
    parser.add_argument("--num_iterations", type=int, default=75)
    parser.add_argument("--num_epochs", type=int, default=5)
    parser.add_argument("--model_dir", type=str, default="output/")
    parser.add_argument("--load_egs", type=str, default="exp/pytorch_egs/")
    parser.add_argument("--load_model", type=str, default="")
    parser.add_argument("--acoustic_model", type=str, default="")
    parser.add_argument("--feature_size", type=int, default=40)
    parser.add_argument("--output_size", type=int, default=3)
    parser.add_argument("--lr", type=float, default=0.001)
    parser.add_argument("--weight_decay", type=float, default=0.0001)
    parser.add_argument("--mode", type=str, default="train-test")
    parser.add_argument("--rollback_threshold", type=int, default=5)
    
    args = parser.parse_args()

    if args.task == "tempo" :
        args.data = "/data/coros1/qraymondaud/voxceleb/hires_tempo_disturb/"
        task = 3

    elif args.task == "emotion" or args.task == "sentiment" :
        args.data = "/data/coros1/qraymondaud/MELD/meld-mfcc-hires"
        if args.task == 'emotion':
            task = 2
            args.output_size=7
        elif args.task == 'sentiment':
            task = 3
            args.output_size=3
        args.data  += "-"+str(task)

    if args.embeddings != "" :

        if args.embeddings == "w2vCNN":
            args.data +="/wav2vec"
            args.max_seq_len = int(args.max_seq_len/2)
            args.feature_size = 512

        elif args.embeddings == 'w2vTRSFMR':
            args.data +="/wav2vec/TRSFMR"
            args.max_seq_len = int(args.max_seq_len/2)
            args.feature_size = 768

    return args

if __name__ == '__main__':

    # Baseline - Setup
    nnet = 0
    feature_type = "MFCC-Hires"

    # Args - Setup
    args = parse_args()
    sys.path.append(args.kaldi_pybind_path)
    import kaldi

    # Inference in Acoustic DNN - Setup
    if ( args.acoustic_model != ""):
        nnet = kaldi.nnet3.ComputePytorch(args.acoustic_model)
        feature_type = args.acoustic_model.split("/")[-1].split(".")[0]
    if ( args.embeddings != ""):
        feature_type = args.embeddings

    msg = str(sys.argv[0]) + "\n" + str(args)
    logger=Logger([args.model_dir+"/"+feature_type+"_train_log_pid-"+str(os.getpid())+".txt"])
    logger.log(content=msg)

    train_test(args)

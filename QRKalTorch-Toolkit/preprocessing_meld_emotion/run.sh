#!/usr/bin/env bash


## Usage : 
## - sbatch run.sh task=1 => Speaker
## - sbatch run.sh task=2 => Emotion
## - sbatch run.sh task=3 => Sentiment

#SBATCH --job-name=ekam-ccfm
#SBATCH --ntasks=1
#SBATCH --partition=cpuonly
#SBATCH --cpus-per-task=1
#SBATCH --mem=5G
#SBATCH --time=10-00:00:00
#SBATCH --mail-type=ALL
#SBATCH --exclude=alpos,talos,idyie

cd ..

. ./path.sh
. ./cmd.sh

for ARGUMENT in "$@"
do
   KEY=$(echo $ARGUMENT | cut -f1 -d=)

   KEY_LENGTH=${#KEY}
   VALUE="${ARGUMENT:$KEY_LENGTH+1}"

   export "$KEY"="$VALUE"
done

audio=/data/coros1/qraymondaud/MELD/
bvx=$audio/meld-mfcc-hires-$task

for splitdirectory in DEV_WAV TEST_WAV TRAIN_WAV
do
  res_vx=$bvx/$splitdirectory

  mkdir -p $bvx
  rm -rf $res_vx
  mkdir $res_vx

  touch $res_vx/wav.scp
  touch $res_vx/utt2spk
  touch $res_vx/spk2utt

  for file in `find $audio'WAV/'$splitdirectory -name '*.wav' -type f`
  do
    a=`echo $file | cut -d'/' -f8-`
    echo $a
    filename=${a////}
    uttname=${filename/.wav/}
    sentiment=$(echo $uttname | cut -d'_' -f$task)
    echo ${sentiment}_$(echo $uttname | cut -d'_' -f1,4-) ${file} >> $res_vx/wav.scp
    echo ${sentiment}_$(echo $uttname | cut -d'_' -f1,4-) ${sentiment} >> $res_vx/utt2spk
  done

  ## Build spk2utt & Compute MFCC

  python3 preprocessing_meld_emotion/build_spkutt.py $res_vx/utt2spk > $res_vx/spk2utt
  #python3 make_trials.py $res_vx/utt2spk $res_vx/vox.trials
  
  utils/fix_data_dir.sh $res_vx
  steps/make_mfcc.sh --write-utt2num-frames true --mfcc-config conf/mfcc_hires.conf --nj 5 --cmd "$train_cmd" $res_vx
  #sid/compute_vad_decision.sh --nj 40 --cmd "$train_cmd" $res_vx
  utils/fix_data_dir.sh $res_vx

done
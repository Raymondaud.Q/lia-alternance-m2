# Compte rendu seconde période entreprise  22-11-21 à 03-12-21

# Lundi 22 novembre

* Rdv avec Micka pour première mission :
	* Récupération d'un modèle acoustique kaldi entrainé sur librispeech.   
	* Découpage du modèle acoustique toutes les 4 couches affines.  
	* Test Inférence sur les modèles découpés avec MFCC Kaldi hires sur d'UrbanSound.  

#  Mardi 23 novembre

* Rdv avec Richard :
	* FAIRE schéma de tout le pipeline.
	* Essayer d'entrainer le modèle acoustique sur d'autres choses que les phonèmes et refaire pareil que job 1.
* Création du pipeline entre modèle acoustique kaldi et speechbrain UrbanSound.
	* Le but est d'envoyer la sortie du modèle kaldi dans le modèle UrbanSound.

# Mercredi 25 Novembre jusqu'à fin de semaine 

* Schéma 
* Debuggage première mission dans le cluster ( Temps de calculs trop longs ) 

![schéma](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/raw/master/RAW/Kaldi_Acoustic_Into_SpeechBrain_UrbanSound.jpg)


# Lundi 29 Novembre

* Installation de l'environrement et lancement de la premiere mission sur desktop du LABO ( Kaldi Pybind11 + Speechbrain )
* Mise à jour du compte rendu
* Insertion schéma 
* Début deuxième mission :
	* Entrainer l'ecapa-tdnn ( Kaldi Pybind 11  + Pytorch ) fourni sur les mfcc hires de voxceleb 

# Mardi 30 Novembre

* Préparation des données et de l'environnement pour la deuxième mission :
	* Entraîner un modèle de reconnaissance du locuteur 
	* Ré-entrainé le modèle avec la sortie issue de divers couches cachées

# Mercredi 1 Décembre 

*  Visio avec Fabrice Lefevre, Mickael Rouvier et Richard Dufour :
	* Point sur les activités faite durant ce début d'alternance
	* Conseils pour oral et rapport
	* Fiche de contact entreprise pour secretariat 
* Debugging et lancement de la deuxième mission ( Kaldi PyTorch Voxceleb )


# Jeudi 2 Décembre

* Attente de résultats : impossible de lancer plus d'experience 
* Début rédaction du rapport d'alternance
* Récupération de résultats 

# Vendredi 3 Décembre 

* Expérience Kaldi Voxceleb Deuxième mission interrompu ( raison inconnue )
* Arret experience Première mission sur cluster 
	* Presque 8 Jours pour 1 FOLD sur cluster
	* 2 Jours pour presque 7 FOLD pc de fonction
		* Lancement des expériences uniquement sur le PC de fonction
* Aggregation de résultats intermédiaires sur le repo git 
* Rédaction du rapport
* Ajout chargement du modèle dans la recette Voxceleb KalTorch
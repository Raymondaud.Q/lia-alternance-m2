# Compte rendu seconde période entreprise  27-09-21 à 08-10-21

# Court Terme 

## Kaldi X Speechbrain : UrbanSound

### Setup, problèmes & solutions

* Installation Kaldi - **( Done 27-09 )**

* Regroupage des .wav de **UrbanSound8K** dans un dossier audio/all/ - **( Done 27-09 )**

* Création du [script de génération de wav.scp, spk2utt et utt2spk](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/blob/master/SpeechBrain-UrbanSound-Recipe/exp_scripts/kaldi-feats/kaldi-param-maker.sh) - **( Done 28-09 )**

* Recherche pour remplacer les feature du recipe UrbanSound-SpeechBrain par les mfcc faites avec Kaldi - **( Start 29-09 )**

	* Recherche à propos problème dans `raw_mfcc_train.5 et 7.scp`   ark/scp empéchant leur lecture avec kaldi_io - **( Start 29-09 )**

		* Recherche Kaldi à propos de la commande `steps/make_mfcc.sh --mfcc-config conf/mfcc_hires.conf --nj 40 --cmd "$train_cmd" data/train`- **( Start 29-09 )**

			* **Solution** : Création de mfcc non compressées avec la commande suivante : `steps/make_mfcc.sh --compress false --mfcc-config conf/mfcc_hires.conf --nj 40 --cmd "$train_cmd" data/train` - **( Done 30-09 )**

	* Recherche pour problème `steps/make_mfcc.sh --mfcc-config conf/mfcc_hires.conf --nj 40 --cmd "$train_cmd" data/train` que 5000 utterances en sortie au lieu de 87XX - **( Done 30-09 )**

		* **SOLUTION** : Construire wav.scp de la sorte : `$label#$spk sox $path -c 1 -b 16 -r 16000 -t wav - |`

			* sox fusionne tout en 1 canal, downgrade le bit-rate à 16 et le sample-rate à 16kHz.
			

* Utiliser speechbrain avec les paramétres accoustiques de Kaldi ( Remplacer dans train.py ) - **( Start 30-09 )**

	* Remplacer la fichier speechbrain/speechbrain/dataio/batch.py par le [Custom batch.py](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/blob/master/SpeechBrain-UrbanSound-Recipe/exp_scripts/kaldi-feats/batch.py) - **( Done 31-09 )**

	* Copier UrbanSound/SoundClassifier/hparams/train_ecapa_tdnn.yaml et le transformer en [train_ecapa_tdnn_woa_kaldi.yaml](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/blob/master/SpeechBrain-UrbanSound-Recipe/exp_scripts/hparams/train_ecapa_tdnn_woa_kaldi.yaml) - **( Done 31-09 )**

	* Copier UrbanSound/SoundClassifier/train.py et editer la copie pour injecter les feature kaldi comme dans [train_with_kaldi_mfcc.py](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/blob/master/SpeechBrain-UrbanSound-Recipe/exp_scripts/kaldi-feats/train_with_kaldi_mfcc.py) - **( Done 31-09 )**

	* Lancer la recette - **( Done 01-10 )**

		* Problème Plis 1 et 2 interrompus - **( Done 04-10 )** 

			* Hypothèse 1 : Intérferences entre plusieurs expérience sur des fichier train.json, valid.json et test.json - **( Done 04-10 )**

				* **Solution hypothétique FAUSSE** : Relancer sans autre expérience UrbanSound ( ou modifier le path des fichiers ci-dessus ) - **( Done 04-10 )**
			
			* Recherche de solutions au problème - **( Start 04-10 )**  


### Lancer la recette 

* Voir [README.md](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/blob/master/SpeechBrain-UrbanSound-Recipe/exp_scripts/README.md)

## 1er Recette : Interpretation SpeechBrain UrbanSound8K


* Expérience UrbanSound8k sans data augmentation. Le but est d'avoir un résultat intermédiaire. Cela nous permettra lorsqu'on va utiliser les paramètres acoustiques de kaldi de pouvoir mieux comparer - **( Done 27-09 )**

* Experience UrbanSound8k 10FOLD avec et sans data augmentation - **( Done 28-09 )**

	* Problème sur certains plis : besoin d'être relancée 

* Interpretation des résultats [UrbanSound8K](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/tree/master/SpeechBrain-UrbanSound-Recipe)


## 2eme Recette : SpeechBrain Voxceleb 

* Relancer la recette sur plusieurs GPU - **( Done 27-09 )**

* Relancer la recette après interruption inexpliquée - **( Done 29-09 )**

* Interpretation des résultats



# Long terme

* Extraire les information au niveau des couches cachées d'un modèle accoustique fourni  :
* Réapprendre UrbanSound et Voxceleb avec les données données par les couches cachées.
* Modifier pour chercher les informations : éviter de lancer MFCC et FILTERBANK à la volée les charger depuis un repertoire   
* Wav2Vec : Manière non supervisée d'app représentation acoustique 
* détection de l'émotion : https://ecs.utdallas.edu/research/researchlabs/msp-lab/MSP-Podcast.html 
* détection de la vitesse de parole/speaking rate : avec sox tu peux augmenter ou diminuer la vitesse de lecture
* détection du genre (homme/femme)
* détection age du locuteur : https://arxiv.org/pdf/2109.13510
* détection de bruit
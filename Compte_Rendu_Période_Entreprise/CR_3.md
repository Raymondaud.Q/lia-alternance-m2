# Compte rendu seconde période entreprise  25-10-21 à 05-11-21

# Lundi 25 octobre 

* Installation Wrapper kaldi pour inserer feature kaldi dans Speechbrain: https://github.com/kaldi-asr/kaldi/tree/pybind11
	* [**Détails de la recette et des dépendances**](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/blob/master/SpeechBrain-UrbanSound-Recipe/exp_scripts/README.md)
	* Possibilité de modifier avec PyKaldi : https://github.com/pykaldi/pykaldi
* Récuperation de la version de M. Rouvier utilisant pybind11 

# Mardi 26 octobre 

* Récupération, modification et lancement du script de création des MFCC avec kaldi
* Configuration hparams expérience MFCC avec kaldi
* Lancement de la recette UrbanSound8k avec les MFCC de Kaldi
* Lecture : https://arxiv.org/pdf/2109.13510

# Mercredi 27 octobre 

* Relancement de UrbanSound avec Data Augmentation sur les plis failed
* Investigation lenteur Voxceleb :
	* CPU saturé, GPU varie entre 0 et 50%
		* Pre-traitement CPU trop important et non parralélisé
			* Recherche d'alternatives
* Récupération des résultats de quelques Fold Kaldi MFCC

# Jeudi 28 octobre 

* Récupération des résultats de quelques Fold Kaldi MFCC
* Relancement de la recette UrbanSound avec Data augmentation
* Relancement de la recette Voxceleb avec hparams/num_workers set à plus de  2 :
	* https://github.com/speechbrain/speechbrain/issues/990

# Vendredi 29 

* Résultats complets UrbanSound avec MFCC KALDI 40 Coeffs et Default MFCC Speechbrain :
	* [**Resultats recette speechbrain par défaut**](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/blob/master/SpeechBrain-UrbanSound-Recipe/10_Fold/SANS_DATA_AUGMENTATION/README.md)
	* [**Resultats recette Kaldi MFCC X Speechbrain**](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/tree/master/SpeechBrain-UrbanSound-Recipe/10_Fold/SANS_DATA_AUGMENTATION_KALDI_MFCC/README.md)
	* [**Comparaison**](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/tree/master/SpeechBrain-UrbanSound-Recipe/10_Fold/README.md)
* Suppression et annulation de la recette avec Data Augmentation
* Interruption de Voxceleb => Problème CPU Bottleneck modification architecture nécessaire : https://github.com/speechbrain/speechbrain/issues/990
* Détection de l'Émotion : https://ecs.utdallas.edu/research/researchlabs/msp-lab/MSP-Podcast.html 
* Voxceleb avec le corpus annoté : https://arxiv.org/pdf/2109.13510 avec pas beaucoup d'epoch

# Lundi 1 Férié 

# Mardi 2 Novembre

* https://deeplizard.com/learn/video/ZjM_XQa5s6s
* https://en.wikipedia.org/wiki/Time_delay_neural_network
* https://web.archive.org/web/20180306041537/https://pdfs.semanticscholar.org/ced2/11de5412580885279090f44968a428f1710b.pdf
* Réunion :
	* EER => Egal entre miss et fa : MISS = Meme locuteur mais faux, FA = Pas le meme mais vrai
	* iteration range = setp_size_up + step_size_down
	* Speechbrain détection d'émotion a faire à partir d'un corpus en anglais : 
		* Il y a Att-HACK, un corpus avec 4 émotions (friendly, seductive, dominant, and distant) : http://www.openslr.org/88/ => French
		* On a aussi les corpus suivant, mais je ne les ai pas encore cherché sur le Web : IEMOCAP, RECOLA, SEWA, CMU-MOSEI
		* Aussi, Jarod a trouvé un lien sympa : https://superkogito.github.io/SER-datasets/

# Mercredi 3 Novembre

* Inspection de la recette d'analyse de sentiment sur IEMOCAP avec Speechbrain : https://github.com/speechbrain/speechbrain/tree/develop/recipes/IEMOCAP
* Corpus MELD : https://github.com/declare-lab/MELD, https://affective-meld.github.io/
	* Lecture du papier relatif au corpus : https://arxiv.org/pdf/1810.02508.pdf


# Court terme

* détection de l'émotion : https://ecs.utdallas.edu/research/researchlabs/msp-lab/MSP-Podcast.html 
* détection age du locuteur : https://arxiv.org/pdf/2109.13510

# Long terme

* Extraire les information au niveau des couches cachées d'un modèle accoustique fourni  :
* Réapprendre UrbanSound et Voxceleb avec les données données par les couches cachées.
* Modifier pour chercher les informations : éviter de lancer MFCC et FILTERBANK à la volée les charger depuis un repertoire   
* Wav2Vec : Manière non supervisée d'app représentation acoustique 
* détection de la vitesse de parole/speaking rate : avec sox tu peux augmenter ou diminuer la vitesse de lecture
* détection du genre (homme/femme)
* détection de bruit
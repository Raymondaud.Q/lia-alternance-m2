# Compte rendu première période entreprise 1-09-21 à 10-09-21


# Installation de l'environement local et distant

* Création d'un environement virtuel en Python 3.8 avec conda  :
	* conda create -n env python=3.8
	* conda activate env 
* [Installation de speechbrain dans env](https://speechbrain.readthedocs.io/en/latest/installation.html)
* Prise en main du cluster
* Prise en main SLURM

# 1er Recette : SpeechBrain UrbanSound8K

* Mise en application de la recette [UrbanSound8K](https://github.com/speechbrain/speechbrain/tree/develop/recipes/UrbanSound8k) dans le cluster

# 2eme Recette : SpeechBrain Voxceleb

* Mise en application de la recette [Voxceleb](https://github.com/speechbrain/speechbrain/tree/develop/recipes/VoxCeleb/SpeakerRec) 

* Conversion de [voxceleb/voxceleb2/dev](https://www.robots.ox.ac.uk/~vgg/data/voxceleb/) de **m4a à wav**

* Application de la partie [ECAPA-TDNN](https://deepai.org/publication/ecapa-tdnn-emphasized-channel-attention-propagation-and-aggregation-in-tdnn-based-speaker-verification) de la recette avec la partie **voxceleb2/dev** du dataset
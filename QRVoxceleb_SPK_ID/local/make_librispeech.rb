#!/usr/bin/env ruby

require "rubygems"
require "json"

def launch

  dir_in = "db/LibriSpeech/train-other-500/"
  dir_out = "data_fbank60/librispeech-train-other-500"

  wav = File.open("#{dir_out}/wav.scp", "w")
  utt2spk = File.open("#{dir_out}/utt2spk", "w")
  spk2utt = File.open("#{dir_out}/spk2utt", "w")

  spk = Hash.new


  d = Dir.glob("#{dir_in}/**/*.flac")
  d.each do |file|
    file_basename = File.basename(file, ".flac")
    id = file_basename.split("-")[0]

    wav << "#{id}\##{file_basename} flac -c -d -s #{file} |\n"
    utt2spk << "#{id}\##{file_basename} #{id}\n"
    spk[ id ] ||= Array.new
    spk[ id ].push( "#{id}\##{file_basename}" )
  end


  spk.each do |key, value|
    spk2utt << "#{key} #{value.join(" ")}\n"
  end

  wav.close
  utt2spk.close
  spk2utt.close

end


def errarg
    puts "Usage : ./programme.rb"
    puts "Mickael Rouvier <mickael.rouvier@gmail.com>"
end


if ARGV.size == 0
    launch
else
    errarg
end


#!/usr/bin/env ruby

require "rubygems"
require "json"

def launch

  dir_in = "/local_disk/arges/rouvier/snips/db2/hey_snips_fl_amt"
  dir_out = "data_fbank60/snips"


  wav = File.open("#{dir_out}/wav.scp", "w")
  utt2spk = File.open("#{dir_out}/utt2spk", "w")
  spk2utt = File.open("#{dir_out}/spk2utt", "w")

  spk = Hash.new

  file = File.read("#{dir_in}/train.json")
  json  = JSON.parse(file)

  #{"duration"=>4.352, "worker_id"=>"1f69ded998b21ed9198f7d0bb3e2bc6e", "audio_file_path"=>"audio_files/1f6f9285-c898-4eb9-87ed-738a4c6e9d39.wav", "id"=>"1f6f9285-c898-4eb9-87ed-738a4c6e9d39", "is_hotword"=>0}

  json.each do |ar|
    wav << "#{ar["worker_id"]}\##{ar["id"]} #{dir_in}/#{ar["audio_file_path"]}\n"
    utt2spk << "#{ar["worker_id"]}\##{ar["id"]} #{ar["worker_id"]}\n"
    spk[ ar["worker_id"] ] ||= Array.new()
    spk[ ar["worker_id"] ].push("#{ar["worker_id"]}\##{ar["id"]}")
  end


  spk.each do |key, value|
    spk2utt << "#{key} #{value.join(" ")}\n"
  end

  wav.close
  utt2spk.close
  spk2utt.close

end


def errarg
    puts "Usage : ./programme.rb"
    puts "Mickael Rouvier <mickael.rouvier@gmail.com>"
end


if ARGV.size == 0
    launch
else
    errarg
end


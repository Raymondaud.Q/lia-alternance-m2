#!/usr/bin/env bash

# Copyright     2017  David Snyder
#               2017  Johns Hopkins University (Author: Daniel Povey)
#               2017  Johns Hopkins University (Author: Daniel Garcia Romero)
# Apache 2.0.

# This script extracts embeddings (called "xvectors" here) from a set of
# utterances, given features and a trained DNN.  The purpose of this script
# is analogous to sid/extract_ivectors.sh: it creates archives of
# vectors that are used in speaker recognition.  Like ivectors, xvectors can
# be used in PLDA or a similar backend for scoring.

# Begin configuration section.
nj=30
cmd="run.pl"

stage=0

echo "$0 $@"  # Print the command line for logging

if [ -f path.sh ]; then . ./path.sh; fi
. parse_options.sh || exit 1;

if [ $# < 3 ]; then
  echo "Usage: $0 <nnet-dir> <data> <xvector-dir>"
  echo " e.g.: $0 exp/xvector_nnet data/train exp/xvectors_train"
  echo "main options (for others, see top of script file)"
  echo "  --config <config-file>                           # config containing options"
  echo "  --cmd (utils/run.pl|utils/queue.pl <queue opts>) # how to run jobs."
  echo "  --nj <n|30>                                      # Number of jobs"
  echo "  --stage <stage|0>                                # To control partial reruns"
  echo "                                                   # chunk size, and averages to produce final embedding"
fi

srcdir=$1
data=$2
dir=$3
acoustic_model=$4
feat_size=$5

#echo ${srcdir} ${data} ${dir} ${acoustic_model} ${feat_size}

for f in $srcdir/final.mat $data/feats.scp $data/vad.scp ; do
  [ ! -f $f ] && echo "No such file $f" && exit 1;
done


nnet=$srcdir/final.mat
nnet_retrain=$srcdir/final_retrain.mat
#--retrain ${nnet_retrain} 
top=$srcdir/final_top.mat

utils/split_data.sh $data $nj
echo "$0: extracting xvectors for $data"
sdata=$data/split$nj/JOB

if [ $stage -le 0 ]; then
  echo "$0: extracting xvectors from nnet"
    $cmd JOB=1:$nj ${dir}/log/extract.JOB.log \
      python3 local/pytorch/extract_baseline_ecapatdnn_good.py\
        --data $data/split$nj/JOB\
        --model $nnet\
        --xvector "ark,scp:${dir}/xvector.JOB.ark,${dir}/xvector.JOB.scp"\
        --kaldi_pybind_path /users/qraymondaud/kaldi/src/pybind\
        --acoustic_model /data/coros1/qraymondaud/acoustic_models/${acoustic_model}.raw\
        --feature_size ${feat_size}
fi

if [ $stage -le 1 ]; then
  echo "$0: combining xvectors across jobs"
  for j in $(seq $nj); do cat $dir/xvector.$j.scp; done >$dir/xvector.scp || exit 1;
fi



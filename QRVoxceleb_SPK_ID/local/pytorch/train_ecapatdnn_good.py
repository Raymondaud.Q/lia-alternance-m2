import os
import sys
import glob
import numpy as np
import argparse
import asyncio
import time
import math
from sklearn.preprocessing import LabelEncoder
from collections import OrderedDict, Counter
from torch.utils.data import Dataset
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
from joblib import Parallel, delayed

def cosine_lr(optimizer, base_lr, e, epochs):
    lr = 0.1 * base_lr * (math.cos(math.pi * e / epochs) + 1)
    for params in optimizer.param_groups:
        params['lr'] = lr

def schedule_lr(optimizer, factor=0.1):
    for params in optimizer.param_groups:
        params['lr'] *= factor

def set_lr(optimizer, factor=0.1):
    for params in optimizer.param_groups:
        params['lr'] = factor

def get_lr(optimizer):
    for param_group in optimizer.param_groups:
        return param_group['lr']

def read_mat(name):
    feat = kaldi.read_mat(name).numpy()
    return feat 

def load_n_col(file, numpy=False):
    data = []
    with open(file) as fp:
        for line in fp:
            data.append(line.strip().split(' '))
    columns = list(zip(*data))
    if numpy:
        columns = [np.array(list(i)) for i in columns]
    else:
        columns = [list(i) for i in columns]
    return columns


def odict_from_2_col(file, numpy=False):
    col0, col1 = load_n_col(file, numpy=numpy)
    return OrderedDict({c0: c1 for c0, c1 in zip(col0, col1)})

def load_one_tomany(file, numpy=False):
    one = []
    many = []
    with open(file) as fp:
        for line in fp:
            line = line.strip().split(' ', 1)
            one.append(line[0])
            m = line[1].split(' ')
            many.append(np.array(m) if numpy else m)
    if numpy:
        one = np.array(one)
    return one, many

def train_transform(feats, seqlen):
    leeway = feats.shape[0] - seqlen
    startslice = np.random.randint(0, int(leeway)) if leeway > 0  else 0
    feats = feats[startslice:startslice+seqlen] if leeway > 0 else np.pad(feats, [(0,-leeway), (0,0)], 'constant')
    return torch.FloatTensor(feats)


async def get_item_train(instructions):
    fpath = instructions[0]
    seqlen = instructions[1]
    raw_feats = read_mat(fpath)
    feats = train_transform(raw_feats, seqlen)
    return feats


async def get_item_test(filepath):
    raw_feats = read_mat(filepath)
    return torch.FloatTensor(raw_feats)


def async_map(coroutine_func, iterable):
    loop = asyncio.get_event_loop()
    future = asyncio.gather(*(coroutine_func(param) for param in iterable))
    return loop.run_until_complete(future)

class SpeakerDataset(Dataset):
    def __init__(self, data_base_path, asynchr=False, num_workers=4):
        self.data_base_path = data_base_path
        self.num_workers = num_workers
        utt2spk_path = os.path.join(data_base_path, 'utt2spk')
        spk2utt_path = os.path.join(data_base_path, 'spk2utt')
        feats_scp_path = os.path.join(data_base_path, 'feats.scp')

        assert os.path.isfile(utt2spk_path)
        assert os.path.isfile(feats_scp_path)
        assert os.path.isfile(spk2utt_path)

        self.utts, self.uspkrs = load_n_col(utt2spk_path)
        self.utt_fpath_dict = odict_from_2_col(feats_scp_path)
        self.label_enc = LabelEncoder()
        self.spkrs, self.spkutts = load_one_tomany(spk2utt_path)
        self.spkrs = self.label_enc.fit_transform(self.spkrs)
        self.spk_utt_dict = OrderedDict({k:v for k,v in zip(self.spkrs, self.spkutts)})
        self.uspkrs = self.label_enc.transform(self.uspkrs)
        self.utt_spkr_dict = OrderedDict({k:v for k,v in zip(self.utts, self.uspkrs)})
        self.utt_list = list(self.utt_fpath_dict.keys())
        self.first_batch = True
        self.num_classes = len(self.label_enc.classes_)
        self.asynchr = asynchr
        self.allowed_classes = self.spkrs.copy() # classes the data can be drawn from
        self.idpool = self.allowed_classes.copy()
        self.ignored = []

    def __len__(self):
        return len(self.utt_list)

    @staticmethod
    # Ici on utilise, s'il existe, notre modèle acoustique kaldi pour transformer nos features
    # Dont le chemin vers le fichier est passé dans l'argument --acoustic_model et désérialisé dans le main dans la variable nnet
    def get_item(instructions): 
        fpath = instructions[0]
        seqlen = instructions[1]
        feats=0
        if ( nnet != 0 ):
            feats = kaldi.read_mat(fpath)
            feats = np.matrix(nnet.GetOutput(feats))
        else:
            feats = read_mat(fpath)
        feats = train_transform(feats, seqlen)
        return feats


    def set_remaining_classes(self, remaining:list):
        self.allowed_classes = sorted(list(set(remaining)))
        self.ignored = sorted(set(np.arange(self.num_classes)) - set(remaining))
        self.idpool = self.allowed_classes.copy()

    def set_ignored_classes(self, ignored:list):
        self.ignored = sorted(list(set(ignored)))
        self.allowed_classes = sorted(set(np.arange(self.num_classes)) - set(ignored))
        self.idpool = self.allowed_classes.copy()

    def set_remaining_classes_comb(self, remaining:list, combined_class_label):
        remaining.append(combined_class_label)
        self.allowed_classes = sorted(list(set(remaining)))
        self.ignored = sorted(set(np.arange(self.num_classes)) - set(remaining))
        self.idpool = self.allowed_classes.copy()        
        for ig in self.ignored:
            # modify self.spk_utt_dict[combined_class_label] to contain all the ignored ids utterances
            self.spk_utt_dict[combined_class_label] += self.spk_utt_dict[ig]
        self.spk_utt_dict[combined_class_label] = list(set(self.spk_utt_dict[combined_class_label]))

    def get_batches(self, batch_size=256, max_seq_len=400):
        # with Parallel(n_jobs=self.num_workers) as parallel:
        #Metric learning assumption large num classes
        assert batch_size < len(self.allowed_classes) 
        lens = [max_seq_len for _ in range(batch_size)]
        while True:
            if len(self.idpool) <= batch_size:
                batch_ids = np.array(self.idpool)
                self.idpool = self.allowed_classes.copy()
                rem_ids = np.random.choice(self.idpool, size=batch_size-len(batch_ids), replace=False)
                batch_ids = np.concatenate([batch_ids, rem_ids])
                self.idpool = list(set(self.idpool) - set(rem_ids))
            else:
                batch_ids = np.random.choice(self.idpool, size=batch_size, replace=False)
                self.idpool = list(set(self.idpool) - set(batch_ids))

            batch_fpaths = []
            for i in batch_ids:
                utt = np.random.choice(self.spk_utt_dict[i])
                batch_fpaths.append(self.utt_fpath_dict[utt])

            if self.asynchr:
                batch_feats = async_map(get_item_train, zip(batch_fpaths, lens))
            else:
                batch_feats = [self.get_item(a) for a in zip(batch_fpaths, lens)]
                #batch_feats = Parallel(n_jobs=self.num_workers)(delayed(self.get_item)(a) for a in zip(batch_fpaths, lens))
            yield torch.stack(batch_feats), list(batch_ids)

class AdditiveAngularMargin(nn.Module):
    def __init__(self, num_features, num_classes, margin=0.2, scale=30.0, easy_margin=False):
        super(AdditiveAngularMargin, self).__init__()
        self.margin = margin
        self.scale = scale
        self.easy_margin = easy_margin
        self.cos_m = math.cos(self.margin)
        self.sin_m = math.sin(self.margin)
        self.th = math.cos(math.pi - self.margin)
        self.mm = math.sin(math.pi - self.margin) * self.margin
        self.margin = margin
        self.scale = scale
        self.fc_weights = torch.nn.Parameter(torch.FloatTensor(num_classes, num_features))
        self.reset_parameters()

    def reset_parameters(self):
        nn.init.xavier_uniform_(self.fc_weights)

    def forward(self, x, targets):
        cosine = F.linear(F.normalize(x), F.normalize(self.fc_weights))
        sine = torch.sqrt(1.0 - torch.pow(cosine, 2))
        phi = cosine * self.cos_m - sine * self.sin_m  # cos(theta + m)
        if self.easy_margin:
            phi = torch.where(cosine > 0, phi, cosine)
        else:
            phi = torch.where(cosine > self.th, phi, cosine - self.mm)
        #outputs = (targets * phi) + ((1.0 - targets) * cosine)
        one_hot = torch.zeros_like(cosine)
        one_hot.scatter_(1, targets.view(-1, 1).long(), 1)
        output = (one_hot * phi) + ((1.0 - one_hot) * cosine)
        output = output * self.scale
        return output


class TDNNBlock(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size=1, stride=1, padding=0, dilation=1, groups=1, bias=True):
        super(TDNNBlock, self).__init__()
        self.conv = nn.Conv1d(in_channels, out_channels, kernel_size=kernel_size, stride=stride, padding=padding, dilation=dilation, groups=groups, bias=bias)
        self.norm = nn.BatchNorm1d(out_channels)

    def forward(self, x):
        return self.norm(F.relu(self.conv(x)))

class Res2NetBlock(torch.nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size=1, stride=1, padding=0, dilation=1, bias=True, scale=4, groups=1):
        super(Res2NetBlock, self).__init__()
        self.scale = scale
        self.in_channel = in_channels // scale
        self.hidden_channel = out_channels // scale
        self.nums = scale if scale == 1 else scale - 1
        self.convs = []
        self.bns = []
        num_pad = math.floor(kernel_size/2)*dilation
        for i in range(self.nums):
            #self.convs.append(nn.Conv1d(self.in_channel, self.hidden_channel, kernel_size=kernel_size, stride=stride, padding=num_pad, dilation=dilation, bias=bias))
            self.convs.append(nn.Conv1d(self.in_channel, self.hidden_channel, kernel_size=kernel_size, stride=stride, padding=padding, dilation=dilation, bias=bias))
            self.bns.append(nn.BatchNorm1d(self.hidden_channel))
        self.convs = nn.ModuleList(self.convs)
        self.bns = nn.ModuleList(self.bns)

    def forward(self, x):
        out = []
        spx = torch.split(x, self.in_channel, 1)
        for i in range(self.nums):
            if i == 0:
                sp = spx[i]
            else:
                sp = sp + spx[i]
            sp = self.convs[i](sp)
            sp = self.bns[i](F.relu(sp))
            out.append(sp)
        if self.scale != 1:
            out.append(spx[self.nums])
        out = torch.cat(out, dim=1)
        return out

'''
        y = []
        for i, x_i in enumerate(torch.chunk(x, self.scale, dim=1)):
            if i == 0:
                y_i = x_i
            elif i == 1:
                y_i = self.bns[i - 1](F.relu(self.convs[i - 1](x_i)))
            else:
                y_i = self.bns[i - 1](F.relu(self.convs[i - 1](x_i + y_i)))
            y.append(y_i)
        y = torch.cat(y, dim=1)
        return y
'''

class SEBlock(nn.Module):
    def __init__(self, in_channels, se_channels, out_channels):
        super(SEBlock, self).__init__()
        self.linear1 = nn.Linear(in_channels, se_channels)
        self.linear2 = nn.Linear(se_channels, out_channels)

    def forward(self, x):
        out = x.mean(dim=2)
        out = F.relu(self.linear1(out))
        out = torch.sigmoid(self.linear2(out))
        out = x * out.unsqueeze(2)
        return out


class SERes2NetBlock(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size=1, stride=1, padding=0, dilation=1, scale=8, se_channels=128, groups=1):
        super(SERes2NetBlock, self).__init__()

        self.tdnnblock1 = TDNNBlock(in_channels, out_channels, kernel_size=1, stride=1, padding=0)
        self.res2netblock = Res2NetBlock(in_channels, out_channels, kernel_size=kernel_size, stride=stride, padding=padding, dilation=dilation, scale=scale)
        self.tdnnblock2 = TDNNBlock(in_channels, out_channels, kernel_size=1, stride=1, padding=0)
        self.se_block = SEBlock(in_channels, se_channels, out_channels)
        self.shortcut = None
        if in_channels != out_channels:
            self.shortcut = Conv1d(in_channels, out_channels, kernel_size=1)

    def forward(self, x):
        residual = x
        if self.shortcut:
            residual = self.shortcut(x)
        x = self.tdnnblock1(x)
        x = self.res2netblock(x)
        x = self.tdnnblock2(x)
        x = self.se_block(x)

        return x + residual


class ECAPA_TDNN(torch.nn.Module):
    def __init__(self, input_size=40, embedding=192, channels=[1024, 1024, 1024, 1024, 3072], kernel_sizes=[5, 3, 3, 3, 1], dilations=[1, 2, 3, 4, 1], groups=[1, 1, 1, 1, 1], attention_channels=128, res2net_scale=8, global_context=True):
        super(ECAPA_TDNN, self).__init__()
        self.layer1 = TDNNBlock(input_size, channels[0], kernel_size=kernel_sizes[0], padding=2, groups=groups[0])
        self.layer2 = SERes2NetBlock(channels[0], channels[1], kernel_size=kernel_sizes[1], stride=1, padding=2, dilation=dilations[1], scale=res2net_scale, se_channels=attention_channels, groups=groups[1])
        self.layer3 = SERes2NetBlock(channels[1], channels[2], kernel_size=kernel_sizes[2], stride=1, padding=3, dilation=dilations[2], scale=res2net_scale, se_channels=attention_channels, groups=groups[2])
        self.layer4 = SERes2NetBlock(channels[2], channels[3], kernel_size=kernel_sizes[3], stride=1, padding=4, dilation=dilations[3], scale=res2net_scale, se_channels=attention_channels, groups=groups[3])
        self.conv = nn.Conv1d(channels[-1], input_size, kernel_size=1)
        self.attention = nn.Sequential(
            nn.Conv1d(input_size, 128, kernel_size=1),
            nn.BatchNorm1d(128),
            nn.Tanh(),
            nn.Conv1d(128, input_size, kernel_size=1),
            nn.Softmax(dim=2),
        )
        self.norm_stats = torch.nn.BatchNorm1d(2*input_size)
        self.fc_embed = nn.Linear(2*input_size, embedding)
        for m in self.modules():
            if isinstance(m, nn.Conv1d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
            elif isinstance(m, nn.BatchNorm1d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

    def forward(self, x):
        x = x.transpose(1, 2)
        out1 = self.layer1(x)
        out2 = self.layer2(out1)
        out3 = self.layer3(out2)
        out4 = self.layer4(out3)
        out = torch.cat([out2, out3, out4], dim=1)
        out = F.relu(self.conv(out))
        w = self.attention(out)
        mu = torch.sum(out * w, dim=2)
        sg = torch.sqrt( ( torch.sum((out**2) * w, dim=2) - mu**2 ).clamp(min=1e-5) )
        out = torch.cat([mu,sg], dim=1)
        out = self.norm_stats(out)
        out = self.fc_embed(out)
        return out

def train(args):
    device = torch.device("cuda")
    use_cuda = not args.use_cuda and torch.cuda.is_available()
    print('='*30)
    print('USE_CUDA SET TO: {}'.format(use_cuda))
    print('CUDA AVAILABLE?: {}'.format(torch.cuda.is_available()))
    print('='*30)

    print(args.load_model)
    model = ECAPA_TDNN(input_size=args.feature_size) 
    if ( args.load_model != "") :
        model.load_state_dict( torch.load(args.model_dir+"/"+feature_type+"_optimizer"+args.load_model+".mat", map_location={"cuda" : "cpu"}).module.state_dict())
    model = nn.DataParallel(model)
    model.to(f'cuda:{model.device_ids[0]}')

    amsmloss = AdditiveAngularMargin(192, 6000) 
    if ( args.load_model != "") :
        amsmloss.load_state_dict( torch.load(args.model_dir+"/"+feature_type+"_top"+args.load_model+".mat", map_location={"cuda" : "cpu"}).module.state_dict())
    amsmloss = nn.DataParallel(amsmloss)
    amsmloss.to(f'cuda:{amsmloss.device_ids[0]}')

    optimizer = torch.optim.Adam([{'params': model.parameters(), 'lr':0.001, 'weight_decay':0.00002}, {'params': amsmloss.parameters(), 'lr':0.001, 'weight_decay':0.0002}])
    criterion = nn.CrossEntropyLoss()

    ds_train = SpeakerDataset(args.data)
    data_generator = ds_train.get_batches(batch_size=args.batch_size, max_seq_len=args.max_seq_len)
    running_loss = [np.nan for _ in range(500)]
    model.train()
    scheduler = torch.optim.lr_scheduler.CyclicLR(optimizer, 0.00000001, 0.001, step_size_up=65000, step_size_down=65000, cycle_momentum=False)


    for epochs in range(1):
        for iterations in range(args.num_iterations):
            optimizer.step()
            feats, iden = next(data_generator)
            feats = feats.to(f'cuda:{model.device_ids[0]}')
            iden = torch.LongTensor(iden).to(f'cuda:{model.device_ids[0]}')
            emb = model(feats)
            preds = amsmloss(emb, iden)
            loss = criterion(preds, iden)
            optimizer.zero_grad()
            loss.backward()
            running_loss.pop(0)
            running_loss.append(loss.item())
            rmean_loss = np.nanmean(np.array(running_loss))

            if iterations % 10000 == 0 or iterations == args.num_iterations-1 :
                torch.save(model, args.model_dir+"/"+feature_type+"_optimizer"+str(epochs*args.num_iterations+iterations)+".mat")
                torch.save(amsmloss, args.model_dir+"/"+feature_type+"_top"+str(epochs*args.num_iterations+iterations)+".mat")
                msg = "{}: {}: [{}/{}] \t C-Loss:{:.4f}, AvgLoss:{:.4f}, lr: {}, bs: {}".format(args.model_dir, time.ctime(), iterations, args.num_iterations, loss.item(), rmean_loss, optimizer.param_groups[0]["lr"], len(feats))
                print(msg)
            scheduler.step()
def parse_args():
    parser = argparse.ArgumentParser(description="Train model")
    parser.add_argument("--kaldi_pybind_path", type=str, default="/Please_Provide --kaldi_pybind_path")
    parser.add_argument("--data", type=str, default="data/train/")
    parser.add_argument("--use_cuda", type=bool, default=True)
    parser.add_argument("--multi_gpu", type=bool, default=True)
    parser.add_argument("--batch_size", type=int, default=256)
    parser.add_argument("--max_seq_len", type=int, default=400)
    parser.add_argument("--num_iterations", type=int, default=130000)
    parser.add_argument("--model_dir", type=str, default="output/")
    parser.add_argument("--load_egs", type=str, default="exp/pytorch_egs/")
    parser.add_argument("--load_model", type=str, default="")
    parser.add_argument("--acoustic_model", type=str, default="")
    parser.add_argument("--feature_size", type=int, default="40")
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = parse_args()
    sys.path.append(args.kaldi_pybind_path)
    import kaldi
    nnet = 0
    feature_type = "MFCC-Hires"
    if ( args.acoustic_model != ""):
        nnet = kaldi.nnet3.ComputePytorch(args.acoustic_model)
        feature_type = args.acoustic_model.split("/")[-1].split(".")[0]
    train(args)

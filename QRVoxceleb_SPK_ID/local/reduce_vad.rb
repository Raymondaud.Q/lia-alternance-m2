#!/usr/bin/env ruby

require "rubygems"

def launch

  $stdin.each do |line|
    line.chomp!
    line = line.split(" ")

    number = line.size-3
    ar = Array.new(number, 0)

    max = 700
    max = number if number < max
    0.upto(700) do |counter|
      ar[counter] = line[counter+2].to_i
    end

    puts "#{line[0]}  [ #{ar.join(" ")} ]"
  end



end


def errarg
    puts "Usage : ./programme.rb"
    puts "Mickael Rouvier <mickael.rouvier@gmail.com>"
end


if ARGV.size == 0
    launch
else
    errarg
end


# What ?

This directory implements an ECAPA-TDNN with PyTorch & Kaldi.  
Kaldi is used for preprocessings only.  
Once preprocessings done, we train the model with PyTorch.  
The pytorch side is based on [M. Rouvier Speaker_Embedding GitHub Repo](https://github.com/mrouvier/speaker_embedding) for NN purposes.  

![schema](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/raw/master/Documents/Kaldi_Acoustic_Into_Voxceleb-ECAPA-TDNN.png)

# Dependencies

* Kaldi Pybind 11 : [ Follow this Readme  ](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/tree/master/SpeechBrain-UrbanSound-Recipe/exp_scripts/kaldi_mfcc_recipe)

* Install[ PyTorch ](https://pytorch.org/)

# Recipe - Voxceleb Baseline

## Starter Pack

* Run :
	* `cd exp_script`
	* `. path.sh`
	* `./run.sh`
	* `./mfcc-merge.sh` (To obtain feats.scp with VAD )
	* `utils/copy_data_dir.sh data/voxceleb/voxceleb2_train/ data/voxceleb/voxceleb2_train_cmvn/`
	* `apply-cmvn-sliding --norm-vars=false --center=true --cmn-window=300 scp:data/voxceleb2_train/feats.scp ark,scp:data/voxceleb/voxceleb2_train_cmvn/feats.ark,data/voxceleb/voxceleb2_train_cmvn/feats.scp`

* Run the training script :
	* `sbatch local/pytorch/train_ecapatdnn_good.sh` :
		* Input data are in `data/voxceleb/voxceleb2_train/` by default

* Run eval :  

	1. `cd exp/pytorch_resnet_block256_ecapatdnn_good`
	2. `ln -fs optimizer`\<num\>`.mat final.mat`
	3. `cd ../../`
	4. `./run_evaluation.sh exp/pytorch_resnet_block256_ecapatdnn_good`

## TEST Results :

### MFCC 30 - After 243k iterations on train

**** VoxCeleb1-E Cleaned ****
EER: 2.117%
minDCF(p-target=0.01): 0.2208
minDCF(p-target=0.001): 0.4032

**** VoxCeleb1-H Cleaned ****
EER: 3.802%
minDCF(p-target=0.01): 0.3248
minDCF(p-target=0.001): 0.4819

### MFCC Hires - After 130k iterations on train

**** VoxCeleb1-E Cleaned ****
EER: 1.967%
minDCF(p-target=0.01): 0.2059
minDCF(p-target=0.001): 0.3565

**** VoxCeleb1-H Cleaned ****
EER: 3.505%
minDCF(p-target=0.01): 0.3024
minDCF(p-target=0.001): 0.4784

### MFCC Hires - After 260k iterations on train

**** VoxCeleb1-E Cleaned ****
EER: 1.912%
minDCF(p-target=0.01): 0.2040
minDCF(p-target=0.001): 0.3549

**** VoxCeleb1-H Cleaned ****
EER: 3.537%
minDCF(p-target=0.01): 0.3107
minDCF(p-target=0.001): 0.4611



# Recipe  - Voxceleb X Kaldi Acoustic

## Running

* Run commands :

	* `cd exp_script`
	* `. path.sh`
	* `./run.sh` 
	* `apply-cmvn-sliding --norm-vars=false --center=true --cmn-window=300 scp:data/voxceleb/voxceleb2_train/feats.scp ark,scp:data/voxceleb/voxceleb2_train_cmvn/feats.ark,data/voxceleb/voxceleb2_train_cmvn/feats.scp`
	* `preparestuff.sh` or use `acoustic_model.zip`
		* It splits acoustic model copy on Affine 1 f2 f4 f6 f8 f10 f12 f14 f16 layers.
		* Its the same as the [UrbanSound recipe's preparation script](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/blob/master/SpeechBrain-UrbanSound-Recipe/exp_scripts/kaldi_acoustic_x_speechbrain/preparestuff.sh)
	* `extract_rpz.sh` :
		* Transforms Voxceleb MFCCs using acoustic model splits.	

* Run training script with different representations :

	* `sbatch local/pytorch/train_ecapatdnn_good.sh <model> 1536`
		* `<model>` could be equal to `affine1`, `affinef2`, `affinef4`, `affinef6`, ...
			* Allows to run recipe with features extracted at a specific affine layer (which outputs 1536 values)

* Run eval :  

	1. `cd exp/pytorch_resnet_block256_ecapatdnn_good`
	2. `ln -fs optimizer<num>.mat final.mat`
	3. `cd ../../`
	4. `./run_evaluation.sh exp/pytorch_resnet_block256_ecapatdnn_good final1 1536`
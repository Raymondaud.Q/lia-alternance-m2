#! /bin/bash

#SBATCH --job-name=US-KALDI-PREP
#SBATCH --ntasks=1
#SBATCH --partition=cpuonly
#SBATCH --cpus-per-task=1
#SBATCH --mem=500M
#SBATCH --time=3-00:00:00
#SBATCH --mail-type=ALL
#echo $1
./run-bis.sh $1

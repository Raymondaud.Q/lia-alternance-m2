. path.sh

for i in 1 f2 f4 f6 f8 f10 f12 f14 f16; do
	echo "output-node name=output input=tdnn${i}.affine" > extract.config
	nnet3-copy --nnet-config=extract.config final.mdl final${i}.raw
done
nnet3-copy final.mdl final_pho.raw

mkdir -p /data/acoustic_models/
mv *.raw /data/acoustic_models/
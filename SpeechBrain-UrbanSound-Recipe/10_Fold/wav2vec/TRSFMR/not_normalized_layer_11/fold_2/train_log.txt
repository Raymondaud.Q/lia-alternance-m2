epoch: 1, lr: 4.21e-06 - train loss: 7.82, train acc: 3.01e-01 - valid loss: 7.46, valid acc: 3.62e-01, valid error: 6.36e-01
epoch: 2, lr: 8.43e-06 - train loss: 6.09, train acc: 5.30e-01 - valid loss: 7.48, valid acc: 4.16e-01, valid error: 5.83e-01
epoch: 3, lr: 1.26e-05 - train loss: 5.30, train acc: 6.04e-01 - valid loss: 8.14, valid acc: 3.85e-01, valid error: 6.15e-01
epoch: 4, lr: 1.69e-05 - train loss: 4.78, train acc: 6.46e-01 - valid loss: 7.58, valid acc: 4.28e-01, valid error: 5.72e-01
epoch: 5, lr: 2.11e-05 - train loss: 4.39, train acc: 6.77e-01 - valid loss: 8.24, valid acc: 4.12e-01, valid error: 5.88e-01
epoch: 6, lr: 2.53e-05 - train loss: 4.17, train acc: 6.93e-01 - valid loss: 8.44, valid acc: 3.94e-01, valid error: 6.05e-01
epoch: 7, lr: 2.95e-05 - train loss: 3.89, train acc: 7.13e-01 - valid loss: 8.40, valid acc: 4.05e-01, valid error: 5.95e-01
epoch: 8, lr: 3.37e-05 - train loss: 3.79, train acc: 7.17e-01 - valid loss: 8.38, valid acc: 3.88e-01, valid error: 6.14e-01
epoch: 9, lr: 3.79e-05 - train loss: 3.62, train acc: 7.34e-01 - valid loss: 8.18, valid acc: 4.40e-01, valid error: 5.61e-01
epoch: 10, lr: 4.21e-05 - train loss: 3.55, train acc: 7.41e-01 - valid loss: 7.99, valid acc: 4.76e-01, valid error: 5.24e-01
epoch: 11, lr: 4.64e-05 - train loss: 3.39, train acc: 7.50e-01 - valid loss: 9.36, valid acc: 4.19e-01, valid error: 5.81e-01
epoch: 12, lr: 5.06e-05 - train loss: 3.17, train acc: 7.72e-01 - valid loss: 9.27, valid acc: 4.39e-01, valid error: 5.61e-01
epoch: 13, lr: 5.48e-05 - train loss: 3.13, train acc: 7.71e-01 - valid loss: 8.44, valid acc: 4.52e-01, valid error: 5.47e-01
epoch: 14, lr: 5.90e-05 - train loss: 3.06, train acc: 7.77e-01 - valid loss: 9.03, valid acc: 4.08e-01, valid error: 5.92e-01
epoch: 15, lr: 6.32e-05 - train loss: 2.99, train acc: 7.83e-01 - valid loss: 8.15, valid acc: 4.87e-01, valid error: 5.14e-01
epoch: 16, lr: 6.74e-05 - train loss: 2.81, train acc: 7.92e-01 - valid loss: 9.15, valid acc: 4.58e-01, valid error: 5.42e-01
epoch: 17, lr: 7.17e-05 - train loss: 2.69, train acc: 8.02e-01 - valid loss: 9.21, valid acc: 4.33e-01, valid error: 5.69e-01
epoch: 18, lr: 7.59e-05 - train loss: 2.73, train acc: 7.99e-01 - valid loss: 8.49, valid acc: 4.51e-01, valid error: 5.50e-01
epoch: 19, lr: 8.01e-05 - train loss: 2.61, train acc: 8.06e-01 - valid loss: 9.46, valid acc: 4.39e-01, valid error: 5.61e-01
epoch: 20, lr: 8.43e-05 - train loss: 2.58, train acc: 8.09e-01 - valid loss: 9.02, valid acc: 4.49e-01, valid error: 5.51e-01
epoch: 21, lr: 8.85e-05 - train loss: 2.42, train acc: 8.21e-01 - valid loss: 9.27, valid acc: 4.59e-01, valid error: 5.41e-01
epoch: 22, lr: 9.27e-05 - train loss: 2.40, train acc: 8.23e-01 - valid loss: 9.91, valid acc: 4.24e-01, valid error: 5.75e-01
epoch: 23, lr: 9.69e-05 - train loss: 2.21, train acc: 8.39e-01 - valid loss: 9.66, valid acc: 4.69e-01, valid error: 5.30e-01
epoch: 24, lr: 1.01e-04 - train loss: 2.16, train acc: 8.40e-01 - valid loss: 8.72, valid acc: 4.83e-01, valid error: 5.16e-01
epoch: 25, lr: 1.05e-04 - train loss: 2.08, train acc: 8.48e-01 - valid loss: 9.57, valid acc: 4.64e-01, valid error: 5.36e-01
Epoch loaded: 15, 
 Per Class Accuracy: 
0: 0.720
1: 0.560
2: 0.381
3: 0.230
4: 0.550
5: 0.943
6: 0.330
7: 0.240
8: 0.650
9: 0.450, 
 Confusion Matrix: 
[[72  7  8  2  4  5  0  0  2  0]
 [ 1 56  2 10 22  1  0  3  4  1]
 [ 2  0 16  5  5  1  1  1  7  4]
 [ 4  3  0 23  9  0  0 12 47  2]
 [ 9 16  3  8 55  0  1  0  5  3]
 [ 0  0  0  0  1 33  0  0  0  1]
 [ 1  6  2 10 17  0 30  1 10 14]
 [ 2  2  0 24  9  0  3 24 34  2]
 [ 1  4  4  4  2  0 16 10 78  1]
 [ 8  1  7  0  4  7  0  9 19 45]]
 - test loss: 8.15, test acc: 4.87e-01, test error: 5.14e-01

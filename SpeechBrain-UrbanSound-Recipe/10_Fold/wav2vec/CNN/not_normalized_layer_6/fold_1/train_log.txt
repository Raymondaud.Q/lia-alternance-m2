epoch: 1, lr: 4.21e-06 - train loss: 7.69, train acc: 3.31e-01 - valid loss: 5.68, valid acc: 5.58e-01, valid error: 4.48e-01
epoch: 2, lr: 8.43e-06 - train loss: 4.43, train acc: 6.92e-01 - valid loss: 6.06, valid acc: 5.54e-01, valid error: 4.55e-01
epoch: 3, lr: 1.26e-05 - train loss: 2.42, train acc: 8.58e-01 - valid loss: 5.13, valid acc: 5.95e-01, valid error: 4.10e-01
epoch: 4, lr: 1.69e-05 - train loss: 1.29, train acc: 9.30e-01 - valid loss: 4.78, valid acc: 6.71e-01, valid error: 3.38e-01
epoch: 5, lr: 2.11e-05 - train loss: 7.38e-01, train acc: 9.61e-01 - valid loss: 6.91, valid acc: 4.76e-01, valid error: 5.29e-01
epoch: 6, lr: 2.53e-05 - train loss: 5.58e-01, train acc: 9.71e-01 - valid loss: 4.87, valid acc: 6.79e-01, valid error: 3.30e-01
epoch: 7, lr: 2.95e-05 - train loss: 4.65e-01, train acc: 9.74e-01 - valid loss: 5.67, valid acc: 6.39e-01, valid error: 3.64e-01
epoch: 8, lr: 3.37e-05 - train loss: 4.43e-01, train acc: 9.75e-01 - valid loss: 9.57, valid acc: 2.97e-01, valid error: 7.10e-01
epoch: 9, lr: 3.79e-05 - train loss: 4.27e-01, train acc: 9.75e-01 - valid loss: 5.42, valid acc: 6.72e-01, valid error: 3.37e-01
epoch: 10, lr: 4.21e-05 - train loss: 3.92e-01, train acc: 9.77e-01 - valid loss: 7.96, valid acc: 5.10e-01, valid error: 4.94e-01
epoch: 11, lr: 4.64e-05 - train loss: 4.08e-01, train acc: 9.75e-01 - valid loss: 6.38, valid acc: 6.22e-01, valid error: 3.88e-01
epoch: 12, lr: 5.06e-05 - train loss: 3.93e-01, train acc: 9.76e-01 - valid loss: 8.32, valid acc: 4.91e-01, valid error: 5.17e-01
epoch: 13, lr: 5.48e-05 - train loss: 3.87e-01, train acc: 9.75e-01 - valid loss: 5.79, valid acc: 6.66e-01, valid error: 3.42e-01
epoch: 14, lr: 5.90e-05 - train loss: 3.74e-01, train acc: 9.78e-01 - valid loss: 5.88, valid acc: 6.66e-01, valid error: 3.42e-01
epoch: 15, lr: 6.32e-05 - train loss: 3.65e-01, train acc: 9.78e-01 - valid loss: 6.52, valid acc: 6.38e-01, valid error: 3.65e-01
epoch: 16, lr: 6.74e-05 - train loss: 3.74e-01, train acc: 9.75e-01 - valid loss: 5.70, valid acc: 6.58e-01, valid error: 3.51e-01
epoch: 17, lr: 7.17e-05 - train loss: 3.34e-01, train acc: 9.79e-01 - valid loss: 5.96, valid acc: 6.65e-01, valid error: 3.44e-01
epoch: 18, lr: 7.59e-05 - train loss: 3.43e-01, train acc: 9.78e-01 - valid loss: 7.32, valid acc: 6.28e-01, valid error: 3.81e-01
epoch: 19, lr: 8.01e-05 - train loss: 3.33e-01, train acc: 9.79e-01 - valid loss: 6.23, valid acc: 6.70e-01, valid error: 3.39e-01
epoch: 20, lr: 8.43e-05 - train loss: 3.24e-01, train acc: 9.79e-01 - valid loss: 5.98, valid acc: 6.84e-01, valid error: 3.24e-01
epoch: 21, lr: 8.85e-05 - train loss: 3.03e-01, train acc: 9.81e-01 - valid loss: 6.35, valid acc: 6.50e-01, valid error: 3.60e-01
epoch: 22, lr: 9.27e-05 - train loss: 2.92e-01, train acc: 9.81e-01 - valid loss: 6.66, valid acc: 6.43e-01, valid error: 3.61e-01
epoch: 23, lr: 9.69e-05 - train loss: 2.87e-01, train acc: 9.81e-01 - valid loss: 6.04, valid acc: 6.81e-01, valid error: 3.28e-01
epoch: 24, lr: 1.01e-04 - train loss: 3.02e-01, train acc: 9.80e-01 - valid loss: 6.35, valid acc: 6.94e-01, valid error: 3.12e-01
epoch: 25, lr: 1.05e-04 - train loss: 2.95e-01, train acc: 9.81e-01 - valid loss: 5.61, valid acc: 6.74e-01, valid error: 3.34e-01
Epoch loaded: 24, 
 Per Class Accuracy: 
0: 0.710
1: 0.900
2: 0.889
3: 0.370
4: 0.720
5: 0.942
6: 0.562
7: 0.660
8: 0.542
9: 0.943, 
 Confusion Matrix: 
[[71 10  3  3  3  3  1  2  4  0]
 [ 0 90  0  0  5  1  1  3  0  0]
 [ 0  0 32  0  2  1  0  0  0  1]
 [ 9  9  0 37  5  0 13  2 25  0]
 [ 1  6  0  6 72  6  0  7  2  0]
 [ 0  3  0  1  0 81  0  1  0  0]
 [ 2  1  0  5  0 26 54  4  4  0]
 [ 1  2  5 14  1  5  0 66  5  1]
 [ 0  1  0  3  1  0  6 44 65  0]
 [ 0  0  0  0  0  1  0  1  0 33]]
 - test loss: 6.35, test acc: 6.94e-01, test error: 3.12e-01

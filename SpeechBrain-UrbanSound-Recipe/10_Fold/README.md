


# Class <-> ClassID 

```
'dog_bark' => 0
'children_playing' => 1
'air_conditioner' => 2
'street_music' => 3
'gun_shot' => 4
'siren' => 5
'engine_idling' => 6
'jackhammer' => 7
'drilling' => 8
'car_horn' => 9

```

# 10-FOLD X-Validation Recipes 

* **Available 10FOLD X-Validation experiments**

	* 10FOLD X-Validation Default [UrbanSound - Speechbrain](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/tree/master/SpeechBrain-UrbanSound-Recipe/exp_scripts/default_recipe)
	* 10FOLD X-Validation Default [Kaldi Hires MFCC](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/tree/master/SpeechBrain-UrbanSound-Recipe/exp_scripts/kaldi_mfcc_recipe)
	* 10FOLD X-Validation [Pipeline Kaldi Acoustic Model - Speechbrain UrbanSound ](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/tree/master/SpeechBrain-UrbanSound-Recipe/exp_scripts/kaldi_acoustic_x_speechbrain)

## Usage 
[Harry_Plotter.py](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/blob/master/SpeechBrain-UrbanSound-Recipe/10_Fold/Harry_Plotter.py) allows to browse our results andshows seaborn confusion matrix from each fold and also cumulated confusion matrix.  

* **Requirements** : `pip3 install numpy seaborn`

* **Options** :

	* each => Displays confusion matrixes from each fold
	* cum => Displays only fold-cumulated confusion matrix
	* all => Combination of cum and each
	* none => Only cli display

* **Available results** :

	* 10FOLD X-Validation Default Speechbrain - UrbanSound [WO_DATA_AUGMENTATION/fbanks/](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/tree/master/SpeechBrain-UrbanSound-Recipe/10_Fold/WO_DATA_AUGMENTATION/fbanks)
	* 10FOLD X-Validation Kaldi Hirest : [WO_DATA_AUGMENTATION_KALDI_MFCC/mfcc/](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/tree/master/SpeechBrain-UrbanSound-Recipe/10_Fold/WO_DATA_AUGMENTATION_KALDI_MFCC/mfcc)
	* 10FOLD X-Validation Pipeline Kaldi Acoustic Model - Speechbrain UrbanSound [PIPELINE_KADLI_SPEECHBRAIN/layer](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/tree/master/SpeechBrain-UrbanSound-Recipe/10_Fold/PIPELINE_KADLI_SPEECHBRAIN) with layer in `final1 finalf2 finalf4 finalf6 finalf8 finalf10 finalf12 finalf14 finalf16`

* Usage example : `python3 Harry_Plotter.py PIPELINE_KADLI_SPEECHBRAIN/finalf4 cum`

* **Output** : 

![Averaged confusion matrix](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/raw/master/SpeechBrain-UrbanSound-Recipe/10_Fold/PIPELINE_KADLI_SPEECHBRAIN/finalf4/Sum_Confu_A4.png)

```
			[...]
			_____________________________________________________

			-- Metrics --PIPELINE_KADLI_SPEECHBRAIN finalf4 fold_9
			_______________________________________________________
			dog_bark Precision =                                  0.8526
			dog_bark Recall =                                       0.81
			dog_bark F-measure =                                  0.8308


			children_playing Precision =                          0.8235
			children_playing Recall =                               0.84
			children_playing F-measure =                          0.8317


			air_conditioner Precision =                           0.7812
			air_conditioner Recall =                              0.7812
			air_conditioner F-measure =                           0.7812


			street_music Precision =                              0.7838
			street_music Recall =                                   0.58
			street_music F-measure =                              0.6667


			gun_shot Precision =                                  0.8056
			gun_shot Recall =                                       0.87
			gun_shot F-measure =                                  0.8365


			siren Precision =                                     0.9615
			siren Recall =                                        0.8065
			siren F-measure =                                     0.8772


			engine_idling Precision =                             0.9011
			engine_idling Recall =                                   1.0
			engine_idling F-measure =                              0.948


			jackhammer Precision =                                0.8065
			jackhammer Recall =                                   0.8427
			jackhammer F-measure =                                0.8242


			drilling Precision =                                  0.8621
			drilling Recall =                                     0.9146
			drilling F-measure =                                  0.8876


			car_horn Precision =                                  0.7593
			car_horn Recall =                                       0.82
			car_horn F-measure =                                  0.7885


			Mean Precision                                        0.8337
			Mean Recall                                           0.8265
			Mean F-Measure                                        0.8272
			Accuracy                                               0.826
			________________________________________________________

			-- Metrics --PIPELINE_KADLI_SPEECHBRAIN finalf4 fold_10
			________________________________________________________
			dog_bark Precision =                                  0.6825
			dog_bark Recall =                                       0.86
			dog_bark F-measure =                                  0.7611


			children_playing Precision =                          0.8723
			children_playing Recall =                               0.82
			children_playing F-measure =                          0.8454


			air_conditioner Precision =                           0.8941
			air_conditioner Recall =                                0.76
			air_conditioner F-measure =                           0.8216


			street_music Precision =                              0.8148
			street_music Recall =                                   0.88
			street_music F-measure =                              0.8462


			gun_shot Precision =                                  0.9677
			gun_shot Recall =                                     0.9375
			gun_shot F-measure =                                  0.9524


			siren Precision =                                     0.7206
			siren Recall =                                        0.5904
			siren F-measure =                                      0.649


			engine_idling Precision =                             0.7981
			engine_idling Recall =                                0.8925
			engine_idling F-measure =                             0.8426


			jackhammer Precision =                                0.9474
			jackhammer Recall =                                   0.9375
			jackhammer F-measure =                                0.9424


			drilling Precision =                                  0.8842
			drilling Recall =                                       0.84
			drilling F-measure =                                  0.8615


			car_horn Precision =                                  0.9032
			car_horn Recall =                                     0.8485
			car_horn F-measure =                                   0.875


			Mean Precision                                        0.8485
			Mean Recall                                           0.8366
			Mean F-Measure                                        0.8397
			Accuracy                                              0.8315


			Overall mean Precision 0.7815


			Overall mean Recall 0.7711


			Overall mean F-Measure 0.7667
```

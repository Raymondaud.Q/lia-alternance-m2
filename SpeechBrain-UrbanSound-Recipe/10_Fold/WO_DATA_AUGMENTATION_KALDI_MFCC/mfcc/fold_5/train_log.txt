epoch: 1, lr: 4.21e-06 - train loss: 7.17, train acc: 3.73e-01 - valid loss: 5.84, valid acc: 5.51e-01, valid error: 4.51e-01
epoch: 2, lr: 8.43e-06 - train loss: 4.84, train acc: 6.46e-01 - valid loss: 4.94, valid acc: 6.37e-01, valid error: 3.69e-01
epoch: 3, lr: 1.26e-05 - train loss: 3.65, train acc: 7.51e-01 - valid loss: 4.89, valid acc: 6.55e-01, valid error: 3.54e-01
epoch: 4, lr: 1.69e-05 - train loss: 2.90, train acc: 8.05e-01 - valid loss: 4.67, valid acc: 6.72e-01, valid error: 3.27e-01
epoch: 5, lr: 2.11e-05 - train loss: 2.28, train acc: 8.54e-01 - valid loss: 5.44, valid acc: 6.53e-01, valid error: 3.53e-01
epoch: 6, lr: 2.53e-05 - train loss: 1.91, train acc: 8.79e-01 - valid loss: 7.14, valid acc: 6.33e-01, valid error: 3.66e-01
epoch: 7, lr: 2.95e-05 - train loss: 1.52, train acc: 9.04e-01 - valid loss: 4.71, valid acc: 6.64e-01, valid error: 3.35e-01
epoch: 8, lr: 3.37e-05 - train loss: 1.17, train acc: 9.28e-01 - valid loss: 4.33, valid acc: 7.23e-01, valid error: 2.81e-01
epoch: 9, lr: 3.79e-05 - train loss: 1.00, train acc: 9.39e-01 - valid loss: 6.03, valid acc: 6.78e-01, valid error: 3.21e-01
epoch: 10, lr: 4.21e-05 - train loss: 8.51e-01, train acc: 9.45e-01 - valid loss: 4.60, valid acc: 7.11e-01, valid error: 2.90e-01
epoch: 11, lr: 4.64e-05 - train loss: 7.48e-01, train acc: 9.53e-01 - valid loss: 5.72, valid acc: 6.28e-01, valid error: 3.72e-01
epoch: 12, lr: 5.06e-05 - train loss: 7.85e-01, train acc: 9.48e-01 - valid loss: 5.37, valid acc: 6.43e-01, valid error: 3.63e-01
epoch: 13, lr: 5.48e-05 - train loss: 7.21e-01, train acc: 9.52e-01 - valid loss: 6.02, valid acc: 6.96e-01, valid error: 3.06e-01
epoch: 14, lr: 5.90e-05 - train loss: 6.82e-01, train acc: 9.56e-01 - valid loss: 4.96, valid acc: 7.25e-01, valid error: 2.76e-01
epoch: 15, lr: 6.32e-05 - train loss: 5.74e-01, train acc: 9.64e-01 - valid loss: 6.27, valid acc: 6.68e-01, valid error: 3.38e-01
epoch: 16, lr: 6.74e-05 - train loss: 5.36e-01, train acc: 9.64e-01 - valid loss: 5.30, valid acc: 6.67e-01, valid error: 3.32e-01
epoch: 17, lr: 7.17e-05 - train loss: 5.24e-01, train acc: 9.66e-01 - valid loss: 4.47, valid acc: 7.20e-01, valid error: 2.81e-01
epoch: 18, lr: 7.59e-05 - train loss: 4.68e-01, train acc: 9.70e-01 - valid loss: 4.81, valid acc: 7.06e-01, valid error: 2.98e-01
epoch: 19, lr: 8.01e-05 - train loss: 4.37e-01, train acc: 9.73e-01 - valid loss: 6.81, valid acc: 6.80e-01, valid error: 3.18e-01
epoch: 20, lr: 8.43e-05 - train loss: 3.78e-01, train acc: 9.76e-01 - valid loss: 5.04, valid acc: 7.06e-01, valid error: 2.98e-01
epoch: 21, lr: 8.85e-05 - train loss: 3.98e-01, train acc: 9.72e-01 - valid loss: 5.58, valid acc: 6.81e-01, valid error: 3.24e-01
epoch: 22, lr: 9.27e-05 - train loss: 3.68e-01, train acc: 9.77e-01 - valid loss: 4.21, valid acc: 7.38e-01, valid error: 2.66e-01
epoch: 23, lr: 9.69e-05 - train loss: 2.83e-01, train acc: 9.82e-01 - valid loss: 5.89, valid acc: 6.84e-01, valid error: 3.17e-01
epoch: 24, lr: 1.01e-04 - train loss: 3.49e-01, train acc: 9.76e-01 - valid loss: 5.53, valid acc: 6.71e-01, valid error: 3.31e-01
epoch: 25, lr: 1.05e-04 - train loss: 3.34e-01, train acc: 9.79e-01 - valid loss: 6.36, valid acc: 6.81e-01, valid error: 3.21e-01
Epoch loaded: 22, 
 Per Class Accuracy: 
0: 0.622
1: 0.760
2: 0.770
3: 0.910
4: 0.790
5: 0.950
6: 0.859
7: 0.271
8: 0.858
9: 0.720, 
 Confusion Matrix: 
[[ 61   3   0   0  17   3   6   1   1   6]
 [  3  76   9   1   1   3   1   5   1   0]
 [  0   1  77   0   6   0   3   4   0   9]
 [  0   0   4  91   0   0   0   2   1   2]
 [  0   2   5   1  79   0   2   1   3   7]
 [  0   2   0   0   0  38   0   0   0   0]
 [  2   0   1   7   0   0  61   0   0   0]
 [  0   0   5  56   1   1   1  29  10   4]
 [  0   0   0   1   1   1   0  13 103   1]
 [  0   0   0  12   2   2   5   3   4  72]]
 - test loss: 4.21, test acc: 7.38e-01, test error: 2.66e-01

epoch: 1, lr: 4.21e-06 - train loss: 7.37, train acc: 3.62e-01 - valid loss: 5.51, valid acc: 5.96e-01, valid error: 4.02e-01
epoch: 2, lr: 8.43e-06 - train loss: 5.12, train acc: 6.28e-01 - valid loss: 4.99, valid acc: 6.67e-01, valid error: 3.30e-01
epoch: 3, lr: 1.26e-05 - train loss: 3.93, train acc: 7.31e-01 - valid loss: 5.07, valid acc: 6.45e-01, valid error: 3.54e-01
epoch: 4, lr: 1.69e-05 - train loss: 3.16, train acc: 7.86e-01 - valid loss: 3.87, valid acc: 7.21e-01, valid error: 2.78e-01
epoch: 5, lr: 2.11e-05 - train loss: 2.70, train acc: 8.22e-01 - valid loss: 3.76, valid acc: 7.36e-01, valid error: 2.65e-01
epoch: 6, lr: 2.53e-05 - train loss: 2.26, train acc: 8.49e-01 - valid loss: 3.78, valid acc: 7.46e-01, valid error: 2.52e-01
epoch: 7, lr: 2.95e-05 - train loss: 1.87, train acc: 8.74e-01 - valid loss: 4.66, valid acc: 7.20e-01, valid error: 2.81e-01
epoch: 8, lr: 3.37e-05 - train loss: 1.49, train acc: 9.05e-01 - valid loss: 5.08, valid acc: 6.91e-01, valid error: 3.06e-01
epoch: 9, lr: 3.79e-05 - train loss: 1.34, train acc: 9.11e-01 - valid loss: 5.74, valid acc: 6.26e-01, valid error: 3.73e-01
epoch: 10, lr: 4.21e-05 - train loss: 1.10, train acc: 9.30e-01 - valid loss: 6.82, valid acc: 6.54e-01, valid error: 3.44e-01
epoch: 11, lr: 4.64e-05 - train loss: 1.11, train acc: 9.28e-01 - valid loss: 4.60, valid acc: 7.14e-01, valid error: 2.87e-01
epoch: 12, lr: 5.06e-05 - train loss: 9.36e-01, train acc: 9.40e-01 - valid loss: 4.64, valid acc: 7.34e-01, valid error: 2.66e-01
epoch: 13, lr: 5.48e-05 - train loss: 8.67e-01, train acc: 9.43e-01 - valid loss: 6.35, valid acc: 6.54e-01, valid error: 3.47e-01
epoch: 14, lr: 5.90e-05 - train loss: 8.35e-01, train acc: 9.48e-01 - valid loss: 4.20, valid acc: 7.67e-01, valid error: 2.33e-01
epoch: 15, lr: 6.32e-05 - train loss: 6.74e-01, train acc: 9.57e-01 - valid loss: 5.92, valid acc: 7.26e-01, valid error: 2.73e-01
epoch: 16, lr: 6.74e-05 - train loss: 7.33e-01, train acc: 9.53e-01 - valid loss: 5.54, valid acc: 6.97e-01, valid error: 3.01e-01
epoch: 17, lr: 7.17e-05 - train loss: 6.38e-01, train acc: 9.58e-01 - valid loss: 5.53, valid acc: 6.77e-01, valid error: 3.21e-01
epoch: 18, lr: 7.59e-05 - train loss: 6.06e-01, train acc: 9.61e-01 - valid loss: 4.07, valid acc: 7.70e-01, valid error: 2.29e-01
epoch: 19, lr: 8.01e-05 - train loss: 5.86e-01, train acc: 9.63e-01 - valid loss: 3.88, valid acc: 7.87e-01, valid error: 2.13e-01
epoch: 20, lr: 8.43e-05 - train loss: 5.55e-01, train acc: 9.63e-01 - valid loss: 4.78, valid acc: 7.40e-01, valid error: 2.61e-01
epoch: 21, lr: 8.85e-05 - train loss: 5.49e-01, train acc: 9.64e-01 - valid loss: 5.79, valid acc: 7.06e-01, valid error: 2.94e-01
epoch: 22, lr: 9.27e-05 - train loss: 4.63e-01, train acc: 9.69e-01 - valid loss: 4.02, valid acc: 7.80e-01, valid error: 2.21e-01
epoch: 23, lr: 9.69e-05 - train loss: 4.55e-01, train acc: 9.70e-01 - valid loss: 4.99, valid acc: 7.00e-01, valid error: 3.00e-01
epoch: 24, lr: 1.01e-04 - train loss: 4.74e-01, train acc: 9.68e-01 - valid loss: 4.60, valid acc: 7.60e-01, valid error: 2.41e-01
epoch: 25, lr: 1.05e-04 - train loss: 4.28e-01, train acc: 9.72e-01 - valid loss: 4.39, valid acc: 7.66e-01, valid error: 2.34e-01
Epoch loaded: 19, 
 Per Class Accuracy: 
0: 0.780
1: 0.770
2: 0.812
3: 0.710
4: 0.870
5: 0.968
6: 0.951
7: 0.449
8: 0.902
9: 0.810, 
 Confusion Matrix: 
[[78  5  1  4  0  1  0  4  0  7]
 [11 77  1  2  5  0  1  0  0  3]
 [ 0  0 26  0  1  0  0  0  0  5]
 [10  4  1 71 11  0  0  0  0  3]
 [ 1  4  0  1 87  0  2  0  0  5]
 [ 0  0  1  0  0 30  0  0  0  0]
 [ 2  0  0  0  2  0 78  0  0  0]
 [ 3  0  0 15  0  0  0 40 11 20]
 [ 1  0  0  5  0  0  0  0 74  2]
 [ 0  0  2  4  1  0  0  0 12 81]]
 - test loss: 3.88, test acc: 7.87e-01, test error: 2.13e-01

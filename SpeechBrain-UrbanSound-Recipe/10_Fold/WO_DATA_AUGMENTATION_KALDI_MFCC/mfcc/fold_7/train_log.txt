epoch: 1, lr: 4.21e-06 - train loss: 7.23, train acc: 3.75e-01 - valid loss: 6.33, valid acc: 4.90e-01, valid error: 5.11e-01
epoch: 2, lr: 8.43e-06 - train loss: 4.67, train acc: 6.63e-01 - valid loss: 5.52, valid acc: 5.82e-01, valid error: 4.26e-01
epoch: 3, lr: 1.26e-05 - train loss: 3.61, train acc: 7.49e-01 - valid loss: 5.53, valid acc: 5.79e-01, valid error: 4.24e-01
epoch: 4, lr: 1.69e-05 - train loss: 2.84, train acc: 8.09e-01 - valid loss: 6.63, valid acc: 5.54e-01, valid error: 4.59e-01
epoch: 5, lr: 2.11e-05 - train loss: 2.18, train acc: 8.57e-01 - valid loss: 5.35, valid acc: 6.27e-01, valid error: 3.84e-01
epoch: 6, lr: 2.53e-05 - train loss: 1.80, train acc: 8.82e-01 - valid loss: 5.91, valid acc: 5.95e-01, valid error: 4.18e-01
epoch: 7, lr: 2.95e-05 - train loss: 1.48, train acc: 9.05e-01 - valid loss: 6.68, valid acc: 5.88e-01, valid error: 4.25e-01
epoch: 8, lr: 3.37e-05 - train loss: 1.15, train acc: 9.25e-01 - valid loss: 7.03, valid acc: 5.58e-01, valid error: 4.45e-01
epoch: 9, lr: 3.79e-05 - train loss: 9.92e-01, train acc: 9.40e-01 - valid loss: 7.05, valid acc: 5.78e-01, valid error: 4.20e-01
epoch: 10, lr: 4.21e-05 - train loss: 8.54e-01, train acc: 9.46e-01 - valid loss: 6.63, valid acc: 6.42e-01, valid error: 3.69e-01
epoch: 11, lr: 4.64e-05 - train loss: 8.46e-01, train acc: 9.45e-01 - valid loss: 7.24, valid acc: 6.18e-01, valid error: 3.94e-01
epoch: 12, lr: 5.06e-05 - train loss: 8.00e-01, train acc: 9.48e-01 - valid loss: 6.57, valid acc: 6.17e-01, valid error: 3.95e-01
epoch: 13, lr: 5.48e-05 - train loss: 6.43e-01, train acc: 9.59e-01 - valid loss: 7.53, valid acc: 5.75e-01, valid error: 4.38e-01
epoch: 14, lr: 5.90e-05 - train loss: 5.95e-01, train acc: 9.64e-01 - valid loss: 6.96, valid acc: 6.39e-01, valid error: 3.72e-01
epoch: 15, lr: 6.32e-05 - train loss: 5.28e-01, train acc: 9.67e-01 - valid loss: 7.98, valid acc: 5.79e-01, valid error: 4.19e-01
epoch: 16, lr: 6.74e-05 - train loss: 4.30e-01, train acc: 9.72e-01 - valid loss: 6.91, valid acc: 5.84e-01, valid error: 4.24e-01
epoch: 17, lr: 7.17e-05 - train loss: 4.35e-01, train acc: 9.71e-01 - valid loss: 7.60, valid acc: 5.77e-01, valid error: 4.31e-01
epoch: 18, lr: 7.59e-05 - train loss: 4.38e-01, train acc: 9.71e-01 - valid loss: 7.57, valid acc: 5.74e-01, valid error: 4.34e-01
epoch: 19, lr: 8.01e-05 - train loss: 4.04e-01, train acc: 9.74e-01 - valid loss: 7.49, valid acc: 5.92e-01, valid error: 4.15e-01
epoch: 20, lr: 8.43e-05 - train loss: 4.12e-01, train acc: 9.72e-01 - valid loss: 8.79, valid acc: 6.03e-01, valid error: 4.09e-01
epoch: 21, lr: 8.85e-05 - train loss: 3.83e-01, train acc: 9.75e-01 - valid loss: 6.84, valid acc: 6.17e-01, valid error: 3.90e-01
epoch: 22, lr: 9.27e-05 - train loss: 3.08e-01, train acc: 9.80e-01 - valid loss: 6.64, valid acc: 6.42e-01, valid error: 3.69e-01
epoch: 23, lr: 9.69e-05 - train loss: 3.01e-01, train acc: 9.80e-01 - valid loss: 7.19, valid acc: 5.93e-01, valid error: 4.14e-01
epoch: 24, lr: 1.01e-04 - train loss: 3.16e-01, train acc: 9.80e-01 - valid loss: 7.99, valid acc: 5.95e-01, valid error: 4.18e-01
epoch: 25, lr: 1.05e-04 - train loss: 2.72e-01, train acc: 9.83e-01 - valid loss: 7.38, valid acc: 5.88e-01, valid error: 4.25e-01
Epoch loaded: 22, 
 Per Class Accuracy: 
0: 0.780
1: 0.780
2: 0.821
3: 0.280
4: 1.000
5: 0.594
6: 0.610
7: 0.211
8: 0.600
9: 0.850, 
 Confusion Matrix: 
[[78  1  3  1  2  5  5  1  1  3]
 [ 0 78  0  1  0  9  3  0  4  5]
 [ 0  1 23  0  0  0  0  3  1  0]
 [ 0  4  0 28  0 46  0  0 22  0]
 [ 0  0  0  0 51  0  0  0  0  0]
 [ 2 12  5  7  0 63  0 16  1  0]
 [ 4  5  3  3  0  6 47  0  5  4]
 [ 0  0  0  0  0 43  0 16 17  0]
 [ 1  2  1  9  0 22  0  0 60  5]
 [ 0  7  0  1  0  1  3  0  3 85]]
 - test loss: 6.64, test acc: 6.42e-01, test error: 3.69e-01

epoch: 1, lr: 4.19e-06 - train loss: 7.19, train acc: 3.74e-01 - valid loss: 5.58, valid acc: 6.02e-01, valid error: 3.98e-01
epoch: 2, lr: 8.39e-06 - train loss: 4.70, train acc: 6.62e-01 - valid loss: 5.69, valid acc: 5.64e-01, valid error: 4.36e-01
epoch: 3, lr: 1.26e-05 - train loss: 3.58, train acc: 7.59e-01 - valid loss: 4.78, valid acc: 6.68e-01, valid error: 3.32e-01
epoch: 4, lr: 1.68e-05 - train loss: 2.82, train acc: 8.14e-01 - valid loss: 5.09, valid acc: 6.43e-01, valid error: 3.57e-01
epoch: 5, lr: 2.10e-05 - train loss: 2.24, train acc: 8.53e-01 - valid loss: 5.69, valid acc: 5.61e-01, valid error: 4.38e-01
epoch: 6, lr: 2.52e-05 - train loss: 1.79, train acc: 8.89e-01 - valid loss: 6.03, valid acc: 5.65e-01, valid error: 4.35e-01
epoch: 7, lr: 2.94e-05 - train loss: 1.43, train acc: 9.10e-01 - valid loss: 7.09, valid acc: 5.78e-01, valid error: 4.22e-01
epoch: 8, lr: 3.36e-05 - train loss: 1.29, train acc: 9.17e-01 - valid loss: 7.74, valid acc: 5.66e-01, valid error: 4.34e-01
epoch: 9, lr: 3.78e-05 - train loss: 9.81e-01, train acc: 9.39e-01 - valid loss: 5.46, valid acc: 6.42e-01, valid error: 3.59e-01
epoch: 10, lr: 4.20e-05 - train loss: 9.30e-01, train acc: 9.40e-01 - valid loss: 5.98, valid acc: 6.42e-01, valid error: 3.58e-01
epoch: 11, lr: 4.62e-05 - train loss: 7.87e-01, train acc: 9.47e-01 - valid loss: 6.45, valid acc: 6.11e-01, valid error: 3.89e-01
epoch: 12, lr: 5.04e-05 - train loss: 8.04e-01, train acc: 9.50e-01 - valid loss: 7.22, valid acc: 5.51e-01, valid error: 4.49e-01
epoch: 13, lr: 5.46e-05 - train loss: 6.26e-01, train acc: 9.59e-01 - valid loss: 6.84, valid acc: 6.15e-01, valid error: 3.85e-01
epoch: 14, lr: 5.88e-05 - train loss: 5.61e-01, train acc: 9.65e-01 - valid loss: 7.47, valid acc: 5.76e-01, valid error: 4.24e-01
epoch: 15, lr: 6.30e-05 - train loss: 5.91e-01, train acc: 9.62e-01 - valid loss: 6.23, valid acc: 6.19e-01, valid error: 3.81e-01
epoch: 16, lr: 6.72e-05 - train loss: 5.35e-01, train acc: 9.64e-01 - valid loss: 7.04, valid acc: 6.11e-01, valid error: 3.89e-01
epoch: 17, lr: 7.14e-05 - train loss: 5.00e-01, train acc: 9.68e-01 - valid loss: 6.32, valid acc: 6.43e-01, valid error: 3.58e-01
epoch: 18, lr: 7.56e-05 - train loss: 4.54e-01, train acc: 9.72e-01 - valid loss: 6.59, valid acc: 6.15e-01, valid error: 3.85e-01
epoch: 19, lr: 7.98e-05 - train loss: 3.97e-01, train acc: 9.75e-01 - valid loss: 8.49, valid acc: 5.47e-01, valid error: 4.54e-01
epoch: 20, lr: 8.40e-05 - train loss: 4.02e-01, train acc: 9.74e-01 - valid loss: 6.63, valid acc: 6.28e-01, valid error: 3.72e-01
epoch: 21, lr: 8.82e-05 - train loss: 3.84e-01, train acc: 9.74e-01 - valid loss: 7.28, valid acc: 5.70e-01, valid error: 4.30e-01
epoch: 22, lr: 9.24e-05 - train loss: 3.47e-01, train acc: 9.78e-01 - valid loss: 7.22, valid acc: 5.84e-01, valid error: 4.16e-01
epoch: 23, lr: 9.66e-05 - train loss: 3.67e-01, train acc: 9.78e-01 - valid loss: 6.75, valid acc: 6.02e-01, valid error: 3.98e-01
epoch: 24, lr: 1.01e-04 - train loss: 3.37e-01, train acc: 9.78e-01 - valid loss: 8.64, valid acc: 5.68e-01, valid error: 4.32e-01
epoch: 25, lr: 1.05e-04 - train loss: 3.33e-01, train acc: 9.79e-01 - valid loss: 6.98, valid acc: 6.04e-01, valid error: 3.96e-01
Epoch loaded: 3, 
 Per Class Accuracy: 
0: 0.660
1: 0.850
2: 0.593
3: 0.500
4: 0.690
5: 0.526
6: 0.687
7: 0.888
8: 0.467
9: 0.710, 
 Confusion Matrix: 
[[ 66   6   2   7   2   1   1   3   1  11]
 [  2  85   3   1   2   0   0   0   0   7]
 [  4   4  35   0   2   0   0   2   3   9]
 [  0   1   2  50   0   0   0  20   5  22]
 [  1   6   1   6  69   0  11   2   1   3]
 [  2   0   2   1   0  20   0   0   9   4]
 [  8   1   0   0  40   0 114   3   0   0]
 [  0   0   2   5   0   1   1  95   3   0]
 [  0   1   0  10   3   4   1   7  56  38]
 [  1   1   5   8   1   1   3   5   4  71]]
 - test loss: 4.78, test acc: 6.68e-01, test error: 3.32e-01

# Generated 2021-12-08 from:
# /home/qraymondaud/speechbrain/recipes/UrbanSound8k3/SoundClassification/hparams/train_ecapa_tdnn.yaml
# yamllint disable
# #################################
# Basic training parameters for urban sound classification. We have first a network that
# computes some embeddings. On the top of that, we employ a classifier.
#
# Author:
#  * David Whipps 2021
#  * Ala Eddine Limame 2021
# #################################

# Seed needs to be set at top of yaml, before objects with parameters are made
seed: 1988
__set_seed: !!python/object/apply:torch.manual_seed [1988]

# Set up folders for reading from and writing to
# Dataset must already exist at `audio_data_folder`
data_folder: /home/qraymondaud/data/UrbanSound8k # e.g., /localscratch/UrbanSound8K
open_rir_folder: <data_folder>/RIRS # Change if needed
audio_data_folder: /home/qraymondaud/data/UrbanSound8k/audio
model: final1
kaldi_data_folder: /home/qraymondaud/data/UrbanSound8k/kaldi_extract/final1
# TODO the follwing folder will contain the resampled audio files (mono channel and config SR) to train on
#reasmpled_audio_data_folder: !ref <data_folder>/audio_mono16kHz
output_folder: ./results/kaldi/final1/fold_7
save_folder: ./results/kaldi/final1/fold_7/save
train_log: ./results/kaldi/final1/fold_7/train_log.txt

# Tensorboard logs
use_tensorboard: true
tensorboard_logs_folder: ./results/kaldi/final1/fold_7/tb_logs/

# Path where data manifest files will be stored
train_annotation: /home/qraymondaud/data/UrbanSound8k/manifest/train.json
valid_annotation: /home/qraymondaud/data/UrbanSound8k/manifest/valid.json
test_annotation: /home/qraymondaud/data/UrbanSound8k/manifest/test.json

# To standardize results, UrbanSound8k has pre-separated samples into
# 10 folds for multi-fold validation
train_fold_nums: [1, 2, 3, 4, 5, 6, 8, 9, 10]
valid_fold_nums: [7]
test_fold_nums: [7]
skip_manifest_creation: false

ckpt_interval_minutes: 15 # save checkpoint every N min

# Training parameters
number_of_epochs: 25
batch_size: 32
lr: 0.001
base_lr: 0.00000001
max_lr: 0.001
step_size: 65000
sample_rate: 16000

# Feature parameters
n_mels: 1536
left_frames: 0
right_frames: 0
deltas: false
amp_to_db: false
normalize: true

# Number of classes
out_n_neurons: 10

# Note that it's actually important to shuffle the data here
# (or at the very least, not sort the data by duration)
# Also note that this does not violate the UrbanSound8k "no-shuffle" policy
# because this does not mix samples from folds in train to valid/test, only
# within train or valid, or test
shuffle: true
dataloader_options:
  batch_size: 32
  shuffle: true
  num_workers: 7

# Functions
compute_features: &id001 !new:speechbrain.lobes.features.Fbank
  n_mels: 1536
  left_frames: 0
  right_frames: 0
  deltas: false

embedding_model: &id007 !new:speechbrain.lobes.models.ECAPA_TDNN.ECAPA_TDNN
  input_size: 1536
  channels: [1024, 1024, 1024, 1024, 3072]
  kernel_sizes: [5, 3, 3, 3, 1]
  dilations: [1, 2, 3, 4, 1]
  attention_channels: 128
  lin_neurons: 192

classifier: &id008 !new:speechbrain.lobes.models.ECAPA_TDNN.Classifier
  input_size: 192
  out_neurons: 10

epoch_counter: &id010 !new:speechbrain.utils.epoch_loop.EpochCounter
  limit: 25


augment_wavedrop: &id002 !new:speechbrain.lobes.augment.TimeDomainSpecAugment
  sample_rate: 16000
  speeds: [100]

augment_speed: &id003 !new:speechbrain.lobes.augment.TimeDomainSpecAugment
  sample_rate: 16000
  speeds: [95, 100, 105]

add_rev: &id004 !new:speechbrain.lobes.augment.EnvCorrupt
  openrir_folder: /home/qraymondaud/data/UrbanSound8k/RIRS
  openrir_max_noise_len: 3.0    # seconds
  reverb_prob: 1.0
  noise_prob: 0.0
  noise_snr_low: 0
  noise_snr_high: 15
  rir_scale_factor: 1.0

add_noise: &id005 !new:speechbrain.lobes.augment.EnvCorrupt
  openrir_folder: /home/qraymondaud/data/UrbanSound8k/RIRS
  openrir_max_noise_len: 3.0    # seconds
  reverb_prob: 0.0
  noise_prob: 1.0
  noise_snr_low: 0
  noise_snr_high: 15
  rir_scale_factor: 1.0

add_rev_noise: &id006 !new:speechbrain.lobes.augment.EnvCorrupt
  openrir_folder: /home/qraymondaud/data/UrbanSound8k/RIRS
  openrir_max_noise_len: 3.0    # seconds
  reverb_prob: 1.0
  noise_prob: 1.0
  noise_snr_low: 0
  noise_snr_high: 15
  rir_scale_factor: 1.0


# Definition of the augmentation pipeline.
# If concat_augment = False, the augmentation techniques are applied
# in sequence. If concat_augment = True, all the augmented signals
# # are concatenated in a single big batch.

augment_pipeline: []
concat_augment: true

mean_var_norm: &id009 !new:speechbrain.processing.features.InputNormalization

  norm_type: sentence
  std_norm: false

modules:
  compute_features: *id001
  augment_wavedrop: *id002
  augment_speed: *id003
  add_rev: *id004
  add_noise: *id005
  add_rev_noise: *id006
  embedding_model: *id007
  classifier: *id008
  mean_var_norm: *id009
compute_cost: !new:speechbrain.nnet.losses.LogSoftmaxWrapper
  loss_fn: !new:speechbrain.nnet.losses.AdditiveAngularMargin
    margin: 0.2
    scale: 30

compute_error: !name:speechbrain.nnet.losses.classification_error

opt_class: !name:torch.optim.Adam
  lr: 0.001
  weight_decay: 0.000002

lr_annealing: !new:speechbrain.nnet.schedulers.CyclicLRScheduler
  base_lr: 0.00000001
  max_lr: 0.001
  step_size: 65000

# Logging + checkpoints
train_logger: !new:speechbrain.utils.train_logger.FileTrainLogger
  save_file: ./results/kaldi/final1/fold_7/train_log.txt

error_stats: !name:speechbrain.utils.metric_stats.MetricStats
  metric: !name:speechbrain.nnet.losses.classification_error
    reduction: batch

checkpointer: !new:speechbrain.utils.checkpoints.Checkpointer
  checkpoints_dir: ./results/kaldi/final1/fold_7/save
  recoverables:
    embedding_model: *id007
    classifier: *id008
    normalizer: *id009
    counter: *id010

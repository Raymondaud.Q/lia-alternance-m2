epoch: 1, lr: 4.21e-06 - train loss: 6.42, train acc: 4.78e-01 - valid loss: 4.62, valid acc: 6.71e-01, valid error: 3.34e-01
epoch: 2, lr: 8.43e-06 - train loss: 2.96, train acc: 8.08e-01 - valid loss: 4.34, valid acc: 7.00e-01, valid error: 3.05e-01
epoch: 3, lr: 1.26e-05 - train loss: 1.76, train acc: 8.90e-01 - valid loss: 4.73, valid acc: 7.10e-01, valid error: 2.92e-01
epoch: 4, lr: 1.69e-05 - train loss: 1.25, train acc: 9.24e-01 - valid loss: 4.11, valid acc: 7.56e-01, valid error: 2.47e-01
epoch: 5, lr: 2.11e-05 - train loss: 1.01, train acc: 9.39e-01 - valid loss: 4.47, valid acc: 7.40e-01, valid error: 2.61e-01
epoch: 6, lr: 2.53e-05 - train loss: 9.07e-01, train acc: 9.44e-01 - valid loss: 4.47, valid acc: 7.37e-01, valid error: 2.67e-01
epoch: 7, lr: 2.95e-05 - train loss: 8.67e-01, train acc: 9.45e-01 - valid loss: 5.48, valid acc: 7.23e-01, valid error: 2.82e-01
epoch: 8, lr: 3.37e-05 - train loss: 8.47e-01, train acc: 9.46e-01 - valid loss: 5.98, valid acc: 6.76e-01, valid error: 3.26e-01
epoch: 9, lr: 3.79e-05 - train loss: 7.86e-01, train acc: 9.52e-01 - valid loss: 5.16, valid acc: 7.32e-01, valid error: 2.73e-01
epoch: 10, lr: 4.21e-05 - train loss: 7.36e-01, train acc: 9.53e-01 - valid loss: 4.79, valid acc: 7.21e-01, valid error: 2.81e-01
epoch: 11, lr: 4.64e-05 - train loss: 6.86e-01, train acc: 9.57e-01 - valid loss: 5.05, valid acc: 7.11e-01, valid error: 2.97e-01
epoch: 12, lr: 5.06e-05 - train loss: 6.11e-01, train acc: 9.61e-01 - valid loss: 6.20, valid acc: 6.56e-01, valid error: 3.47e-01
epoch: 13, lr: 5.48e-05 - train loss: 5.24e-01, train acc: 9.66e-01 - valid loss: 4.56, valid acc: 7.26e-01, valid error: 2.78e-01
epoch: 14, lr: 5.90e-05 - train loss: 4.94e-01, train acc: 9.67e-01 - valid loss: 6.25, valid acc: 6.76e-01, valid error: 3.26e-01
epoch: 15, lr: 6.32e-05 - train loss: 4.64e-01, train acc: 9.70e-01 - valid loss: 6.40, valid acc: 6.58e-01, valid error: 3.42e-01
epoch: 16, lr: 6.74e-05 - train loss: 4.71e-01, train acc: 9.68e-01 - valid loss: 5.16, valid acc: 7.44e-01, valid error: 2.57e-01
epoch: 17, lr: 7.17e-05 - train loss: 4.20e-01, train acc: 9.72e-01 - valid loss: 6.30, valid acc: 6.81e-01, valid error: 3.24e-01
epoch: 18, lr: 7.59e-05 - train loss: 4.11e-01, train acc: 9.74e-01 - valid loss: 5.65, valid acc: 6.90e-01, valid error: 3.13e-01
epoch: 19, lr: 8.01e-05 - train loss: 4.12e-01, train acc: 9.75e-01 - valid loss: 7.03, valid acc: 6.83e-01, valid error: 3.20e-01
epoch: 20, lr: 8.43e-05 - train loss: 3.66e-01, train acc: 9.75e-01 - valid loss: 5.46, valid acc: 7.57e-01, valid error: 2.50e-01
epoch: 21, lr: 8.85e-05 - train loss: 3.54e-01, train acc: 9.75e-01 - valid loss: 6.05, valid acc: 7.05e-01, valid error: 2.97e-01
epoch: 22, lr: 9.27e-05 - train loss: 3.18e-01, train acc: 9.79e-01 - valid loss: 6.46, valid acc: 6.68e-01, valid error: 3.34e-01
epoch: 23, lr: 9.69e-05 - train loss: 3.03e-01, train acc: 9.80e-01 - valid loss: 5.59, valid acc: 7.39e-01, valid error: 2.65e-01
epoch: 24, lr: 1.01e-04 - train loss: 2.73e-01, train acc: 9.83e-01 - valid loss: 6.32, valid acc: 6.84e-01, valid error: 3.18e-01
epoch: 25, lr: 1.05e-04 - train loss: 2.93e-01, train acc: 9.80e-01 - valid loss: 5.29, valid acc: 7.37e-01, valid error: 2.67e-01
Epoch loaded: 4, 
 Per Class Accuracy: 
0: 0.850
1: 0.900
2: 1.000
3: 0.690
4: 0.770
5: 0.872
6: 0.573
7: 0.690
8: 0.550
9: 1.000, 
 Confusion Matrix: 
[[85  4  6  2  0  2  0  0  1  0]
 [ 0 90  0  0  6  0  3  1  0  0]
 [ 0  0 36  0  0  0  0  0  0  0]
 [ 1 14  0 69  0  0 14  1  1  0]
 [ 1  9  0  3 77  5  2  1  2  0]
 [ 0  9  0  0  0 75  1  1  0  0]
 [ 0  2  0  8  0  1 55  1 29  0]
 [ 0  0 15  9  2  2  2 69  0  1]
 [ 0  0  0  5  0  0  2 45 66  2]
 [ 0  0  0  0  0  0  0  0  0 35]]
 - test loss: 4.11, test acc: 7.56e-01, test error: 2.47e-01

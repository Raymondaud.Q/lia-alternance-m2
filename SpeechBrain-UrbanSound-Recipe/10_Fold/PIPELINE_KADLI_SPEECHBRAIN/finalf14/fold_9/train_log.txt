epoch: 1, lr: 4.21e-06 - train loss: 6.68, train acc: 4.59e-01 - valid loss: 4.76, valid acc: 6.65e-01, valid error: 3.37e-01
epoch: 2, lr: 8.43e-06 - train loss: 3.34, train acc: 7.80e-01 - valid loss: 4.88, valid acc: 6.72e-01, valid error: 3.31e-01
epoch: 3, lr: 1.26e-05 - train loss: 2.06, train acc: 8.73e-01 - valid loss: 5.11, valid acc: 6.69e-01, valid error: 3.31e-01
epoch: 4, lr: 1.69e-05 - train loss: 1.49, train acc: 9.10e-01 - valid loss: 3.98, valid acc: 7.40e-01, valid error: 2.62e-01
epoch: 5, lr: 2.11e-05 - train loss: 1.20, train acc: 9.24e-01 - valid loss: 4.71, valid acc: 7.14e-01, valid error: 2.88e-01
epoch: 6, lr: 2.53e-05 - train loss: 1.11, train acc: 9.33e-01 - valid loss: 4.41, valid acc: 7.46e-01, valid error: 2.56e-01
epoch: 7, lr: 2.95e-05 - train loss: 9.94e-01, train acc: 9.35e-01 - valid loss: 5.15, valid acc: 7.24e-01, valid error: 2.77e-01
epoch: 8, lr: 3.37e-05 - train loss: 9.95e-01, train acc: 9.36e-01 - valid loss: 4.97, valid acc: 7.18e-01, valid error: 2.84e-01
epoch: 9, lr: 3.79e-05 - train loss: 9.84e-01, train acc: 9.37e-01 - valid loss: 5.08, valid acc: 7.15e-01, valid error: 2.88e-01
epoch: 10, lr: 4.21e-05 - train loss: 8.69e-01, train acc: 9.43e-01 - valid loss: 6.20, valid acc: 6.89e-01, valid error: 3.14e-01
epoch: 11, lr: 4.64e-05 - train loss: 7.99e-01, train acc: 9.48e-01 - valid loss: 4.99, valid acc: 7.39e-01, valid error: 2.62e-01
epoch: 12, lr: 5.06e-05 - train loss: 7.12e-01, train acc: 9.51e-01 - valid loss: 4.53, valid acc: 7.49e-01, valid error: 2.55e-01
epoch: 13, lr: 5.48e-05 - train loss: 6.55e-01, train acc: 9.56e-01 - valid loss: 5.07, valid acc: 7.27e-01, valid error: 2.72e-01
epoch: 14, lr: 5.90e-05 - train loss: 6.10e-01, train acc: 9.59e-01 - valid loss: 4.61, valid acc: 7.55e-01, valid error: 2.49e-01
epoch: 15, lr: 6.32e-05 - train loss: 5.60e-01, train acc: 9.66e-01 - valid loss: 4.66, valid acc: 7.75e-01, valid error: 2.25e-01
epoch: 16, lr: 6.74e-05 - train loss: 5.62e-01, train acc: 9.64e-01 - valid loss: 5.95, valid acc: 7.27e-01, valid error: 2.73e-01
epoch: 17, lr: 7.17e-05 - train loss: 5.15e-01, train acc: 9.67e-01 - valid loss: 5.97, valid acc: 7.30e-01, valid error: 2.72e-01
epoch: 18, lr: 7.59e-05 - train loss: 4.66e-01, train acc: 9.69e-01 - valid loss: 4.61, valid acc: 7.56e-01, valid error: 2.48e-01
epoch: 19, lr: 8.01e-05 - train loss: 5.07e-01, train acc: 9.67e-01 - valid loss: 5.17, valid acc: 7.50e-01, valid error: 2.52e-01
epoch: 20, lr: 8.43e-05 - train loss: 3.84e-01, train acc: 9.76e-01 - valid loss: 5.59, valid acc: 7.46e-01, valid error: 2.57e-01
epoch: 21, lr: 8.85e-05 - train loss: 4.14e-01, train acc: 9.75e-01 - valid loss: 5.21, valid acc: 7.43e-01, valid error: 2.61e-01
epoch: 22, lr: 9.27e-05 - train loss: 3.71e-01, train acc: 9.76e-01 - valid loss: 4.69, valid acc: 7.57e-01, valid error: 2.44e-01
epoch: 23, lr: 9.69e-05 - train loss: 3.63e-01, train acc: 9.77e-01 - valid loss: 5.23, valid acc: 7.36e-01, valid error: 2.65e-01
epoch: 24, lr: 1.01e-04 - train loss: 3.20e-01, train acc: 9.80e-01 - valid loss: 4.86, valid acc: 7.40e-01, valid error: 2.61e-01
epoch: 25, lr: 1.05e-04 - train loss: 3.19e-01, train acc: 9.79e-01 - valid loss: 5.44, valid acc: 7.34e-01, valid error: 2.66e-01
Epoch loaded: 15, 
 Per Class Accuracy: 
0: 0.840
1: 0.810
2: 0.781
3: 0.340
4: 0.820
5: 0.806
6: 0.988
7: 0.787
8: 0.915
9: 0.750, 
 Confusion Matrix: 
[[84  1  2  6  5  0  1  0  0  1]
 [ 3 81  0  1 10  0  2  3  0  0]
 [ 0  0 25  0  0  0  1  5  0  1]
 [27  2  5 34 25  0  0  5  1  1]
 [ 0 15  0  2 82  0  0  0  0  1]
 [ 0  0  0  0  0 25  1  0  0  5]
 [ 0  0  0  0  0  0 81  1  0  0]
 [ 3  1  0  9  3  0  0 70  3  0]
 [ 0  0  0  0  0  0  0  2 75  5]
 [ 0  0  0  0  2  0  1  1 21 75]]
 - test loss: 4.66, test acc: 7.75e-01, test error: 2.25e-01

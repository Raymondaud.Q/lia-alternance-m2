epoch: 1, lr: 4.15e-06 - train loss: 6.70, train acc: 4.50e-01 - valid loss: 4.95, valid acc: 6.40e-01, valid error: 3.67e-01
epoch: 2, lr: 8.36e-06 - train loss: 3.19, train acc: 7.85e-01 - valid loss: 4.64, valid acc: 6.74e-01, valid error: 3.29e-01
epoch: 3, lr: 1.26e-05 - train loss: 1.88, train acc: 8.82e-01 - valid loss: 5.55, valid acc: 6.73e-01, valid error: 3.32e-01
epoch: 4, lr: 1.68e-05 - train loss: 1.30, train acc: 9.22e-01 - valid loss: 5.15, valid acc: 6.84e-01, valid error: 3.22e-01
epoch: 5, lr: 2.10e-05 - train loss: 1.18, train acc: 9.25e-01 - valid loss: 5.09, valid acc: 6.90e-01, valid error: 3.15e-01
epoch: 6, lr: 2.52e-05 - train loss: 9.62e-01, train acc: 9.42e-01 - valid loss: 5.14, valid acc: 7.09e-01, valid error: 2.99e-01
epoch: 7, lr: 4.21e-06 - train loss: 3.90e-01, train acc: 9.78e-01 - valid loss: 4.66, valid acc: 7.39e-01, valid error: 2.68e-01
epoch: 8, lr: 8.43e-06 - train loss: 2.42e-01, train acc: 9.88e-01 - valid loss: 5.52, valid acc: 7.10e-01, valid error: 2.98e-01
epoch: 9, lr: 1.26e-05 - train loss: 2.58e-01, train acc: 9.86e-01 - valid loss: 4.70, valid acc: 7.60e-01, valid error: 2.46e-01
epoch: 10, lr: 1.69e-05 - train loss: 3.26e-01, train acc: 9.83e-01 - valid loss: 5.96, valid acc: 6.69e-01, valid error: 3.37e-01
epoch: 11, lr: 2.11e-05 - train loss: 3.86e-01, train acc: 9.76e-01 - valid loss: 4.63, valid acc: 7.51e-01, valid error: 2.55e-01
epoch: 12, lr: 2.53e-05 - train loss: 4.86e-01, train acc: 9.72e-01 - valid loss: 5.40, valid acc: 6.72e-01, valid error: 3.33e-01
epoch: 13, lr: 2.95e-05 - train loss: 5.50e-01, train acc: 9.66e-01 - valid loss: 5.74, valid acc: 6.76e-01, valid error: 3.30e-01
epoch: 14, lr: 3.37e-05 - train loss: 6.16e-01, train acc: 9.60e-01 - valid loss: 5.44, valid acc: 7.20e-01, valid error: 2.84e-01
epoch: 15, lr: 3.79e-05 - train loss: 6.36e-01, train acc: 9.60e-01 - valid loss: 6.75, valid acc: 6.40e-01, valid error: 3.67e-01
epoch: 16, lr: 4.21e-05 - train loss: 5.58e-01, train acc: 9.64e-01 - valid loss: 5.28, valid acc: 7.41e-01, valid error: 2.66e-01
epoch: 17, lr: 4.64e-05 - train loss: 5.91e-01, train acc: 9.62e-01 - valid loss: 6.27, valid acc: 6.47e-01, valid error: 3.54e-01
epoch: 18, lr: 5.06e-05 - train loss: 5.11e-01, train acc: 9.68e-01 - valid loss: 6.65, valid acc: 6.67e-01, valid error: 3.36e-01
epoch: 19, lr: 5.48e-05 - train loss: 5.57e-01, train acc: 9.64e-01 - valid loss: 5.77, valid acc: 7.14e-01, valid error: 2.93e-01
epoch: 20, lr: 5.90e-05 - train loss: 5.16e-01, train acc: 9.66e-01 - valid loss: 5.21, valid acc: 7.35e-01, valid error: 2.71e-01
epoch: 21, lr: 6.32e-05 - train loss: 4.66e-01, train acc: 9.70e-01 - valid loss: 5.31, valid acc: 7.39e-01, valid error: 2.62e-01
epoch: 22, lr: 6.74e-05 - train loss: 4.59e-01, train acc: 9.71e-01 - valid loss: 5.72, valid acc: 7.24e-01, valid error: 2.83e-01
epoch: 23, lr: 7.17e-05 - train loss: 4.38e-01, train acc: 9.71e-01 - valid loss: 5.49, valid acc: 7.29e-01, valid error: 2.78e-01
epoch: 24, lr: 7.59e-05 - train loss: 3.96e-01, train acc: 9.73e-01 - valid loss: 5.82, valid acc: 7.19e-01, valid error: 2.89e-01
epoch: 25, lr: 8.01e-05 - train loss: 3.99e-01, train acc: 9.73e-01 - valid loss: 6.14, valid acc: 6.98e-01, valid error: 3.10e-01
Epoch loaded: 9, 
 Per Class Accuracy: 
0: 0.870
1: 0.880
2: 1.000
3: 0.830
4: 0.810
5: 0.919
6: 0.469
7: 0.540
8: 0.583
9: 1.000, 
 Confusion Matrix: 
[[87  4  2  2  0  0  2  0  1  2]
 [ 1 88  0  4  5  0  1  1  0  0]
 [ 0  0 36  0  0  0  0  0  0  0]
 [ 8  1  2 83  3  0  3  0  0  0]
 [ 1  2  3  3 81  5  0  0  5  0]
 [ 0  6  0  1  0 79  0  0  0  0]
 [ 0  0  5 13  2  5 45  8 18  0]
 [ 2  0 11 14 10  2  4 54  3  0]
 [ 0  0  0  2  0  0  7 41 70  0]
 [ 0  0  0  0  0  0  0  0  0 35]]
 - test loss: 4.70, test acc: 7.60e-01, test error: 2.46e-01

epoch: 1, lr: 4.21e-06 - train loss: 6.53, train acc: 4.63e-01 - valid loss: 4.90, valid acc: 6.46e-01, valid error: 3.61e-01
epoch: 2, lr: 8.43e-06 - train loss: 2.95, train acc: 8.07e-01 - valid loss: 4.92, valid acc: 6.62e-01, valid error: 3.44e-01
epoch: 3, lr: 1.26e-05 - train loss: 1.78, train acc: 8.87e-01 - valid loss: 5.09, valid acc: 7.02e-01, valid error: 3.06e-01
epoch: 4, lr: 1.69e-05 - train loss: 1.20, train acc: 9.27e-01 - valid loss: 4.88, valid acc: 7.11e-01, valid error: 2.97e-01
epoch: 5, lr: 2.11e-05 - train loss: 1.01, train acc: 9.41e-01 - valid loss: 5.02, valid acc: 6.81e-01, valid error: 3.24e-01
epoch: 6, lr: 2.53e-05 - train loss: 8.98e-01, train acc: 9.42e-01 - valid loss: 5.16, valid acc: 6.99e-01, valid error: 3.09e-01
epoch: 7, lr: 2.95e-05 - train loss: 8.60e-01, train acc: 9.45e-01 - valid loss: 5.80, valid acc: 7.18e-01, valid error: 2.90e-01
epoch: 8, lr: 3.37e-05 - train loss: 8.25e-01, train acc: 9.48e-01 - valid loss: 5.41, valid acc: 7.20e-01, valid error: 2.88e-01
epoch: 9, lr: 3.79e-05 - train loss: 7.53e-01, train acc: 9.51e-01 - valid loss: 4.42, valid acc: 7.48e-01, valid error: 2.59e-01
epoch: 10, lr: 4.21e-05 - train loss: 7.45e-01, train acc: 9.52e-01 - valid loss: 4.56, valid acc: 7.63e-01, valid error: 2.37e-01
epoch: 11, lr: 4.64e-05 - train loss: 6.40e-01, train acc: 9.59e-01 - valid loss: 5.39, valid acc: 7.14e-01, valid error: 2.93e-01
epoch: 12, lr: 5.06e-05 - train loss: 6.15e-01, train acc: 9.61e-01 - valid loss: 5.95, valid acc: 7.00e-01, valid error: 3.08e-01
epoch: 13, lr: 5.48e-05 - train loss: 5.93e-01, train acc: 9.64e-01 - valid loss: 5.84, valid acc: 6.73e-01, valid error: 3.32e-01
epoch: 14, lr: 5.90e-05 - train loss: 5.67e-01, train acc: 9.62e-01 - valid loss: 6.73, valid acc: 6.52e-01, valid error: 3.54e-01
epoch: 15, lr: 6.32e-05 - train loss: 4.68e-01, train acc: 9.70e-01 - valid loss: 5.81, valid acc: 6.90e-01, valid error: 3.13e-01
epoch: 16, lr: 6.74e-05 - train loss: 4.69e-01, train acc: 9.68e-01 - valid loss: 5.07, valid acc: 7.27e-01, valid error: 2.81e-01
epoch: 17, lr: 7.17e-05 - train loss: 4.50e-01, train acc: 9.71e-01 - valid loss: 7.30, valid acc: 6.38e-01, valid error: 3.65e-01
epoch: 18, lr: 7.59e-05 - train loss: 4.41e-01, train acc: 9.71e-01 - valid loss: 6.02, valid acc: 7.07e-01, valid error: 2.98e-01
epoch: 19, lr: 8.01e-05 - train loss: 4.24e-01, train acc: 9.71e-01 - valid loss: 6.28, valid acc: 7.01e-01, valid error: 3.07e-01
epoch: 20, lr: 8.43e-05 - train loss: 3.37e-01, train acc: 9.79e-01 - valid loss: 5.64, valid acc: 7.11e-01, valid error: 2.97e-01
epoch: 21, lr: 8.85e-05 - train loss: 3.03e-01, train acc: 9.81e-01 - valid loss: 6.19, valid acc: 6.94e-01, valid error: 3.14e-01
epoch: 22, lr: 9.27e-05 - train loss: 3.26e-01, train acc: 9.79e-01 - valid loss: 6.64, valid acc: 6.89e-01, valid error: 3.16e-01
epoch: 23, lr: 9.69e-05 - train loss: 3.32e-01, train acc: 9.78e-01 - valid loss: 6.53, valid acc: 7.11e-01, valid error: 2.97e-01
epoch: 24, lr: 1.01e-04 - train loss: 2.81e-01, train acc: 9.83e-01 - valid loss: 6.19, valid acc: 7.17e-01, valid error: 2.91e-01
epoch: 25, lr: 1.05e-04 - train loss: 2.70e-01, train acc: 9.83e-01 - valid loss: 7.19, valid acc: 6.55e-01, valid error: 3.52e-01
Epoch loaded: 10, 
 Per Class Accuracy: 
0: 0.840
1: 0.900
2: 1.000
3: 0.820
4: 0.770
5: 0.942
6: 0.333
7: 0.720
8: 0.650
9: 0.971, 
 Confusion Matrix: 
[[84  4  1  3  4  1  1  2  0  0]
 [ 2 90  0  3  4  0  0  1  0  0]
 [ 0  0 36  0  0  0  0  0  0  0]
 [ 6  9  0 82  1  0  2  0  0  0]
 [ 0  7  0  9 77  4  0  0  3  0]
 [ 0  2  0  1  2 81  0  0  0  0]
 [ 0  0  0 54  0  1 32  0  9  0]
 [ 1  2  1 13  0  2  1 72  7  1]
 [ 0  0  0  0  0  0  0 42 78  0]
 [ 1  0  0  0  0  0  0  0  0 34]]
 - test loss: 4.56, test acc: 7.63e-01, test error: 2.37e-01

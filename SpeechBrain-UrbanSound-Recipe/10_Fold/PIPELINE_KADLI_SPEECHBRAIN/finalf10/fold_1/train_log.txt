epoch: 1, lr: 4.21e-06 - train loss: 6.47, train acc: 4.80e-01 - valid loss: 4.98, valid acc: 6.47e-01, valid error: 3.60e-01
epoch: 2, lr: 8.43e-06 - train loss: 2.97, train acc: 8.05e-01 - valid loss: 4.87, valid acc: 6.79e-01, valid error: 3.30e-01
epoch: 3, lr: 1.26e-05 - train loss: 1.81, train acc: 8.90e-01 - valid loss: 5.17, valid acc: 6.92e-01, valid error: 3.16e-01
epoch: 4, lr: 1.69e-05 - train loss: 1.24, train acc: 9.27e-01 - valid loss: 4.50, valid acc: 7.34e-01, valid error: 2.70e-01
epoch: 5, lr: 2.11e-05 - train loss: 1.06, train acc: 9.37e-01 - valid loss: 4.72, valid acc: 7.30e-01, valid error: 2.74e-01
epoch: 6, lr: 2.53e-05 - train loss: 9.57e-01, train acc: 9.40e-01 - valid loss: 4.65, valid acc: 7.37e-01, valid error: 2.70e-01
epoch: 7, lr: 2.95e-05 - train loss: 9.16e-01, train acc: 9.42e-01 - valid loss: 6.29, valid acc: 6.95e-01, valid error: 3.10e-01
epoch: 8, lr: 3.37e-05 - train loss: 8.88e-01, train acc: 9.41e-01 - valid loss: 5.65, valid acc: 6.87e-01, valid error: 3.15e-01
epoch: 9, lr: 3.79e-05 - train loss: 7.70e-01, train acc: 9.52e-01 - valid loss: 5.85, valid acc: 6.84e-01, valid error: 3.22e-01
epoch: 10, lr: 4.21e-05 - train loss: 7.31e-01, train acc: 9.54e-01 - valid loss: 5.20, valid acc: 7.01e-01, valid error: 3.04e-01
epoch: 11, lr: 4.64e-05 - train loss: 6.58e-01, train acc: 9.57e-01 - valid loss: 5.94, valid acc: 7.00e-01, valid error: 3.02e-01
epoch: 12, lr: 5.06e-05 - train loss: 6.14e-01, train acc: 9.60e-01 - valid loss: 5.19, valid acc: 7.48e-01, valid error: 2.59e-01
epoch: 13, lr: 5.48e-05 - train loss: 5.42e-01, train acc: 9.66e-01 - valid loss: 5.48, valid acc: 7.30e-01, valid error: 2.74e-01
epoch: 14, lr: 5.90e-05 - train loss: 5.55e-01, train acc: 9.63e-01 - valid loss: 5.26, valid acc: 7.14e-01, valid error: 2.91e-01
epoch: 15, lr: 6.32e-05 - train loss: 5.52e-01, train acc: 9.63e-01 - valid loss: 6.60, valid acc: 6.82e-01, valid error: 3.21e-01
epoch: 16, lr: 6.74e-05 - train loss: 4.57e-01, train acc: 9.70e-01 - valid loss: 5.34, valid acc: 7.10e-01, valid error: 2.92e-01
epoch: 17, lr: 7.17e-05 - train loss: 4.24e-01, train acc: 9.73e-01 - valid loss: 4.83, valid acc: 7.37e-01, valid error: 2.67e-01
epoch: 18, lr: 7.59e-05 - train loss: 4.52e-01, train acc: 9.69e-01 - valid loss: 6.16, valid acc: 6.80e-01, valid error: 3.25e-01
epoch: 19, lr: 8.01e-05 - train loss: 4.16e-01, train acc: 9.72e-01 - valid loss: 5.40, valid acc: 7.42e-01, valid error: 2.62e-01
epoch: 20, lr: 8.43e-05 - train loss: 3.80e-01, train acc: 9.75e-01 - valid loss: 4.73, valid acc: 7.50e-01, valid error: 2.57e-01
epoch: 21, lr: 8.85e-05 - train loss: 3.41e-01, train acc: 9.79e-01 - valid loss: 6.30, valid acc: 7.00e-01, valid error: 3.02e-01
epoch: 22, lr: 9.27e-05 - train loss: 3.09e-01, train acc: 9.80e-01 - valid loss: 5.17, valid acc: 7.49e-01, valid error: 2.54e-01
epoch: 23, lr: 9.69e-05 - train loss: 3.22e-01, train acc: 9.79e-01 - valid loss: 6.01, valid acc: 7.10e-01, valid error: 2.94e-01
epoch: 24, lr: 1.01e-04 - train loss: 3.45e-01, train acc: 9.76e-01 - valid loss: 6.56, valid acc: 6.81e-01, valid error: 3.22e-01
epoch: 25, lr: 1.05e-04 - train loss: 3.00e-01, train acc: 9.80e-01 - valid loss: 6.02, valid acc: 7.23e-01, valid error: 2.82e-01
Epoch loaded: 22, 
 Per Class Accuracy: 
0: 0.890
1: 0.870
2: 0.972
3: 0.690
4: 0.790
5: 0.895
6: 0.417
7: 0.780
8: 0.517
9: 1.000, 
 Confusion Matrix: 
[[89  4  2  2  0  2  0  0  1  0]
 [ 2 87  0  1  6  1  1  2  0  0]
 [ 0  0 35  0  1  0  0  0  0  0]
 [ 3  7 11 69  3  0  2  0  4  1]
 [ 0  7  3  1 79  5  1  1  3  0]
 [ 0  1  1  0  4 77  1  2  0  0]
 [ 0  2  0 41  0  1 40  0 12  0]
 [ 3  1  1  5  1  0  6 78  5  0]
 [ 0  0  0  1  0  0  4 53 62  0]
 [ 0  0  0  0  0  0  0  0  0 35]]
 - test loss: 5.17, test acc: 7.49e-01, test error: 2.54e-01

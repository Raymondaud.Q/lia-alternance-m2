epoch: 1, lr: 4.21e-06 - train loss: 6.48, train acc: 4.90e-01 - valid loss: 5.10, valid acc: 6.44e-01, valid error: 3.67e-01
epoch: 2, lr: 8.43e-06 - train loss: 3.06, train acc: 8.01e-01 - valid loss: 4.49, valid acc: 6.90e-01, valid error: 3.15e-01
epoch: 3, lr: 1.26e-05 - train loss: 1.87, train acc: 8.87e-01 - valid loss: 4.45, valid acc: 6.81e-01, valid error: 3.29e-01
epoch: 4, lr: 1.69e-05 - train loss: 1.33, train acc: 9.22e-01 - valid loss: 4.22, valid acc: 7.14e-01, valid error: 2.95e-01
epoch: 5, lr: 2.11e-05 - train loss: 1.14, train acc: 9.31e-01 - valid loss: 5.43, valid acc: 6.41e-01, valid error: 3.65e-01
epoch: 6, lr: 2.53e-05 - train loss: 1.01, train acc: 9.39e-01 - valid loss: 5.41, valid acc: 6.62e-01, valid error: 3.44e-01
epoch: 7, lr: 2.95e-05 - train loss: 9.72e-01, train acc: 9.38e-01 - valid loss: 4.56, valid acc: 7.55e-01, valid error: 2.53e-01
epoch: 8, lr: 3.37e-05 - train loss: 8.76e-01, train acc: 9.43e-01 - valid loss: 5.46, valid acc: 6.92e-01, valid error: 3.08e-01
epoch: 9, lr: 3.79e-05 - train loss: 7.88e-01, train acc: 9.49e-01 - valid loss: 5.67, valid acc: 6.65e-01, valid error: 3.40e-01
epoch: 10, lr: 4.21e-05 - train loss: 7.40e-01, train acc: 9.52e-01 - valid loss: 6.54, valid acc: 6.57e-01, valid error: 3.49e-01
epoch: 11, lr: 4.64e-05 - train loss: 6.90e-01, train acc: 9.55e-01 - valid loss: 5.69, valid acc: 7.06e-01, valid error: 2.98e-01
epoch: 12, lr: 5.06e-05 - train loss: 6.61e-01, train acc: 9.58e-01 - valid loss: 7.62, valid acc: 6.47e-01, valid error: 3.59e-01
epoch: 13, lr: 5.48e-05 - train loss: 6.70e-01, train acc: 9.56e-01 - valid loss: 6.30, valid acc: 6.77e-01, valid error: 3.23e-01
epoch: 14, lr: 5.90e-05 - train loss: 5.34e-01, train acc: 9.66e-01 - valid loss: 5.28, valid acc: 7.68e-01, valid error: 2.39e-01
epoch: 15, lr: 6.32e-05 - train loss: 5.54e-01, train acc: 9.65e-01 - valid loss: 5.54, valid acc: 7.26e-01, valid error: 2.83e-01
epoch: 16, lr: 6.74e-05 - train loss: 5.02e-01, train acc: 9.68e-01 - valid loss: 5.76, valid acc: 7.40e-01, valid error: 2.63e-01
epoch: 17, lr: 7.17e-05 - train loss: 4.13e-01, train acc: 9.73e-01 - valid loss: 6.10, valid acc: 7.09e-01, valid error: 2.95e-01
epoch: 18, lr: 7.59e-05 - train loss: 5.12e-01, train acc: 9.65e-01 - valid loss: 6.65, valid acc: 6.88e-01, valid error: 3.23e-01
epoch: 19, lr: 8.01e-05 - train loss: 3.98e-01, train acc: 9.75e-01 - valid loss: 6.72, valid acc: 6.54e-01, valid error: 3.57e-01
epoch: 20, lr: 8.43e-05 - train loss: 4.17e-01, train acc: 9.74e-01 - valid loss: 5.63, valid acc: 7.11e-01, valid error: 2.93e-01
epoch: 21, lr: 8.85e-05 - train loss: 3.71e-01, train acc: 9.77e-01 - valid loss: 6.36, valid acc: 6.98e-01, valid error: 3.06e-01
epoch: 22, lr: 9.27e-05 - train loss: 3.48e-01, train acc: 9.77e-01 - valid loss: 6.23, valid acc: 7.05e-01, valid error: 2.99e-01
epoch: 23, lr: 9.69e-05 - train loss: 3.40e-01, train acc: 9.78e-01 - valid loss: 6.99, valid acc: 6.75e-01, valid error: 3.30e-01
epoch: 24, lr: 1.01e-04 - train loss: 2.98e-01, train acc: 9.80e-01 - valid loss: 6.34, valid acc: 6.77e-01, valid error: 3.28e-01
epoch: 25, lr: 1.05e-04 - train loss: 2.86e-01, train acc: 9.81e-01 - valid loss: 6.71, valid acc: 6.98e-01, valid error: 3.06e-01
Epoch loaded: 14, 
 Per Class Accuracy: 
0: 0.930
1: 0.690
2: 0.933
3: 0.440
4: 0.740
5: 1.000
6: 0.550
7: 0.818
8: 0.936
9: 0.860, 
 Confusion Matrix: 
[[93  2  0  0  1  0  3  0  0  1]
 [ 2 69  0  6 10  0  4  9  0  0]
 [ 0  1 28  0  0  0  0  1  0  0]
 [ 2 15  0 44 15  0  0 20  3  1]
 [ 6 10  0  0 74  1  1  2  4  2]
 [ 0  0  0  0  0 30  0  0  0  0]
 [ 0  0  1  3  2  0 44 29  0  1]
 [ 2  2  0  6  1  0  0 72  4  1]
 [ 0  0  0  0  1  0  0  0 73  4]
 [ 1  0  0  1  0  1  5  0  6 86]]
 - test loss: 5.28, test acc: 7.68e-01, test error: 2.39e-01

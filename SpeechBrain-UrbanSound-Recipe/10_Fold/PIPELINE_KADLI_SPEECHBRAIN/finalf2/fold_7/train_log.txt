epoch: 1, lr: 4.21e-06 - train loss: 6.78, train acc: 4.46e-01 - valid loss: 5.76, valid acc: 5.40e-01, valid error: 4.64e-01
epoch: 2, lr: 8.43e-06 - train loss: 3.58, train acc: 7.60e-01 - valid loss: 4.73, valid acc: 6.60e-01, valid error: 3.51e-01
epoch: 3, lr: 1.26e-05 - train loss: 2.10, train acc: 8.69e-01 - valid loss: 4.66, valid acc: 6.77e-01, valid error: 3.33e-01
epoch: 4, lr: 1.69e-05 - train loss: 1.49, train acc: 9.09e-01 - valid loss: 5.10, valid acc: 6.90e-01, valid error: 3.20e-01
epoch: 5, lr: 2.11e-05 - train loss: 1.16, train acc: 9.31e-01 - valid loss: 5.18, valid acc: 6.92e-01, valid error: 3.17e-01
epoch: 6, lr: 2.53e-05 - train loss: 1.06, train acc: 9.33e-01 - valid loss: 5.20, valid acc: 6.61e-01, valid error: 3.50e-01
epoch: 7, lr: 2.95e-05 - train loss: 9.53e-01, train acc: 9.41e-01 - valid loss: 5.94, valid acc: 6.69e-01, valid error: 3.41e-01
epoch: 8, lr: 3.37e-05 - train loss: 8.91e-01, train acc: 9.40e-01 - valid loss: 6.04, valid acc: 6.80e-01, valid error: 3.25e-01
epoch: 9, lr: 3.79e-05 - train loss: 8.81e-01, train acc: 9.43e-01 - valid loss: 6.03, valid acc: 6.50e-01, valid error: 3.56e-01
epoch: 10, lr: 4.21e-05 - train loss: 8.14e-01, train acc: 9.50e-01 - valid loss: 5.74, valid acc: 7.03e-01, valid error: 3.07e-01
epoch: 11, lr: 4.64e-05 - train loss: 7.73e-01, train acc: 9.51e-01 - valid loss: 5.20, valid acc: 6.95e-01, valid error: 3.09e-01
epoch: 12, lr: 5.06e-05 - train loss: 7.58e-01, train acc: 9.53e-01 - valid loss: 5.48, valid acc: 7.09e-01, valid error: 3.00e-01
epoch: 13, lr: 5.48e-05 - train loss: 6.02e-01, train acc: 9.61e-01 - valid loss: 5.97, valid acc: 6.76e-01, valid error: 3.29e-01
epoch: 14, lr: 5.90e-05 - train loss: 5.96e-01, train acc: 9.62e-01 - valid loss: 6.09, valid acc: 7.03e-01, valid error: 3.07e-01
epoch: 15, lr: 6.32e-05 - train loss: 5.98e-01, train acc: 9.62e-01 - valid loss: 6.55, valid acc: 6.23e-01, valid error: 3.78e-01
epoch: 16, lr: 6.74e-05 - train loss: 5.51e-01, train acc: 9.63e-01 - valid loss: 6.22, valid acc: 6.79e-01, valid error: 3.21e-01
epoch: 17, lr: 7.17e-05 - train loss: 4.70e-01, train acc: 9.69e-01 - valid loss: 6.26, valid acc: 6.80e-01, valid error: 3.25e-01
epoch: 18, lr: 7.59e-05 - train loss: 5.12e-01, train acc: 9.67e-01 - valid loss: 5.72, valid acc: 7.20e-01, valid error: 2.84e-01
epoch: 19, lr: 8.01e-05 - train loss: 3.69e-01, train acc: 9.75e-01 - valid loss: 5.24, valid acc: 7.14e-01, valid error: 2.95e-01
epoch: 20, lr: 8.43e-05 - train loss: 4.04e-01, train acc: 9.74e-01 - valid loss: 5.85, valid acc: 6.91e-01, valid error: 3.14e-01
epoch: 21, lr: 8.85e-05 - train loss: 3.43e-01, train acc: 9.78e-01 - valid loss: 6.24, valid acc: 6.91e-01, valid error: 3.19e-01
epoch: 22, lr: 9.27e-05 - train loss: 3.71e-01, train acc: 9.76e-01 - valid loss: 5.89, valid acc: 6.94e-01, valid error: 3.10e-01
epoch: 23, lr: 9.69e-05 - train loss: 3.34e-01, train acc: 9.78e-01 - valid loss: 5.99, valid acc: 6.66e-01, valid error: 3.39e-01
epoch: 24, lr: 1.01e-04 - train loss: 2.96e-01, train acc: 9.80e-01 - valid loss: 6.50, valid acc: 6.94e-01, valid error: 3.10e-01
epoch: 25, lr: 1.05e-04 - train loss: 2.64e-01, train acc: 9.84e-01 - valid loss: 6.25, valid acc: 6.74e-01, valid error: 3.31e-01
Epoch loaded: 18, 
 Per Class Accuracy: 
0: 0.830
1: 0.730
2: 0.964
3: 0.860
4: 0.980
5: 0.462
6: 0.610
7: 0.211
8: 0.800
9: 0.890, 
 Confusion Matrix: 
[[83  4  1  3  0  2  2  1  1  3]
 [ 0 73  0 14  0  0  7  0  1  5]
 [ 0  0 27  0  0  0  0  1  0  0]
 [ 0  3  0 86  0  0  0  2  9  0]
 [ 1  0  0  0 50  0  0  0  0  0]
 [ 6  4  3 20  0 49  1 19  4  0]
 [ 3  5  3  2  1  1 47  0  4 11]
 [ 0  0  2 40  0  0  0 16 18  0]
 [ 0  0  2 15  0  0  0  1 80  2]
 [ 0  5  1  1  0  0  1  1  2 89]]
 - test loss: 5.72, test acc: 7.20e-01, test error: 2.84e-01

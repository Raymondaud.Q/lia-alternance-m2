
# UrbanSound Recipe : 10_Fold 

[Results on 10-Fold X-Validation Experiments](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/tree/master/SpeechBrain-UrbanSound-Recipe/10_Fold) 

# Kaldi exp_scripts

[Tools](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/tree/master/SpeechBrain-UrbanSound-Recipe/exp_scripts) every script you need to reproduce these experiments
# Dépot des travaux et des résultats réalisés - M2 Alternance LIA

* [FR : Comptes Rendus période entreprise (Premier Semestre Uniquement)](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/blob/master/Compte_Rendu_Période_Entreprise/)
* [SpeechBrain - UrbanSound8k : Noise Identification ](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/tree/master/SpeechBrain-UrbanSound-Recipe)
* [Kaldi-PyTorch ECAPA-TDNN - Toolkit for MELD & Voxceleb ](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/tree/master/QRKalTorch-Toolkit)
* [Kaldi-PyTorch ECAPA-TDNN Exp - Voxceleb SPKID x-vector extraction ](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/tree/master/QRVoxceleb_SPK_ID)
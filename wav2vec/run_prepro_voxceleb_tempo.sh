#!/usr/bin/env bash

#SBATCH --job-name=ekam-ccfm
#SBATCH --ntasks=1
#SBATCH --partition=cpuonly
#SBATCH --cpus-per-task=1
#SBATCH --mem=5G
#SBATCH --time=10-00:00:00
#SBATCH --mail-type=ALL

. ./cmd.sh
. ./path.sh

audio=/data/coros1/qraymondaud/voxceleb
bvx=$audio/hires_tempo_disturb
res_vx=$bvx/$1

mkdir -p $bvx
rm -rf $res_vx
mkdir $res_vx

touch $res_vx/wav.scp
touch $res_vx/utt2spk
touch $res_vx/spk2utt

for file in `find $audio/$1/wav/ -name '*.wav' -type f`
do

  rdm=$((0 + $RANDOM % 3))
  speed=$([[ $rdm == "2" ]] && echo 1.15 || echo 0.85 )
  speed=$([[ $rdm == "1" ]] && echo 1 || echo $speed )

  a=`echo $file | cut -d'/' -f8-`
  uttname=${a////-}
  uttname=${uttname/.wav/-t$speed}
  spkid=$(echo $uttname | cut -d'-' -f1)
  newfile=${file/.wav/-t$speed}.wav
  sox ${file} ${newfile} tempo ${speed}
  echo "${uttname} ${newfile}" >> $res_vx/wav.scp
  echo "${uttname} ${spkid}" >> $res_vx/utt2spk
done

# Build spk2utt & Compute MFCC
python3 build_spkutt_voxceleb.py $res_vx/utt2spk > $res_vx/spk2utt
#python3 make_trials.py $res_vx/utt2spk $res_vx/vox.trials
utils/fix_data_dir.sh $res_vx
steps/make_mfcc.sh --write-utt2num-frames true --mfcc-config conf/mfcc_hires.conf --nj 20 --cmd "$train_cmd" $res_vx
sid/compute_vad_decision.sh --nj 40 --cmd "$train_cmd" $res_vx
utils/fix_data_dir.sh $res_vx

python3 build_spkutt_voxceleb.py $res_vx/utt2spk speed_spk2utt > $res_vx/spk2utt
python3 build_spkutt_voxceleb.py $res_vx/utt2spk speed_utt2spk > $res_vx/utt2spk2
mv $res_vx/utt2spk $res_vx/utt2spk3
mv $res_vx/utt2spk2 $res_vx/utt2spk

# Delete copied & edited WAV
for file in `find $audio/$1/wav/ -name '*-t*.wav' -type f`
do
  rm $file
done
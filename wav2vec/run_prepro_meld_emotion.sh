#!/usr/bin/env bash


## Usage : 
## - sbatch run.sh task=1 => Speaker
## - sbatch run.sh task=2 => Emotion
## - sbatch run.sh task=3 => Sentiment

#SBATCH --job-name=ekam-ccfm
#SBATCH --ntasks=1
#SBATCH --partition=gpu
#SBATCH --gpus-per-node=1
#SBATCH --mem=5G
#SBATCH --time=10-00:00:00
#SBATCH --mail-type=ALL
#SBATCH --exclude=alpos,talos,idyie


# Example Usage :

  #sbatch run_prepro_meld_emotion.sh task=3 splitdirectory=TRAIN_WAV extraction_level=TRSFMR num_layer=1
  #sbatch run_prepro_meld_emotion.sh task=3 splitdirectory=DEV_WAV extraction_level=TRSFMR num_layer=1
  #sbatch run_prepro_meld_emotion.sh task=3 splitdirectory=TEST_WAV extraction_level=TRSFMR num_layer=1
  #sbatch run_prepro_meld_emotion.sh task=2 splitdirectory=TEST_WAV extraction_level=TRSFMR num_layer=1
  #sbatch run_prepro_meld_emotion.sh task=2 splitdirectory=DEV_WAV extraction_level=TRSFMR num_layer=1
  #sbatch run_prepro_meld_emotion.sh task=2 splitdirectory=TRAIN_WAV extraction_level=TRSFMR num_layer=1

  #sbatch run_prepro_meld_emotion.sh task=3 splitdirectory=TRAIN_WAV extraction_level=CNN num_layer=1
  #sbatch run_prepro_meld_emotion.sh task=3 splitdirectory=DEV_WAV extraction_level=CNN num_layer=1
  #sbatch run_prepro_meld_emotion.sh task=3 splitdirectory=TEST_WAV extraction_level=CNN num_layer=1
  #sbatch run_prepro_meld_emotion.sh task=2 splitdirectory=TEST_WAV extraction_level=CNN num_layer=1
  #sbatch run_prepro_meld_emotion.sh task=2 splitdirectory=DEV_WAV extraction_level=CNN num_layer=1
  #sbatch run_prepro_meld_emotion.sh task=2 splitdirectory=TRAIN_WAV extraction_level=CNN num_layer=1

./path.sh
. ./cmd.sh


for ARGUMENT in "$@"
do
   KEY=$(echo $ARGUMENT | cut -f1 -d=)

   KEY_LENGTH=${#KEY}
   VALUE="${ARGUMENT:$KEY_LENGTH+1}"

   export "$KEY"="$VALUE"
done

audio=/data/coros1/qraymondaud/MELD/
normalize=$([[ -z  $normalize ]] && echo "True" || echo $normalize )
num_layer=$([[ -z  $num_layer ]] && echo "7" || echo  $num_layer )
extraction_level=$([[ -z  $extraction_level ]] && echo "CNN" || echo $extraction_level )
pathartifact=$([ "$normalize" == "False" ] && echo "not_normalized/" || echo "" )
bvx=$audio/meld-mfcc-hires-$task/$pathartifact

for splitdirectory in DEV_WAV TEST_WAV TRAIN_WAV
do
  res_vx=$bvx/$splitdirectory

  #mkdir -p $bvx
  #rm -rf $res_vx
  #mkdir $res_vx

  #touch $res_vx/wav.scp
  #touch $res_vx/utt2spk
  #touch $res_vx/spk2utt

  #for file in `find $audio'WAV/'$splitdirectory -name '*.wav' -type f`
  #do
  #  a=`echo $file | cut -d'/' -f8-`
  #  echo $a
  #  filename=${a////}
  #  uttname=${filename/.wav/}
  #  sentiment=$(echo $uttname | cut -d'_' -f$task)
  #  echo $(echo $uttname | cut -d'_' -f$task-) ${file} >> $res_vx/wav.scp
  #  echo $(echo $uttname | cut -d'_' -f$task-) ${sentiment} >> $res_vx/utt2spk
  #done

  ## Build spk2utt & Compute MFCC

  #python3 build_spkutt_meld.py $res_vx/utt2spk > $res_vx/spk2utt

  utils/fix_data_dir.sh $res_vx
  ./compute-w2v2-features.sh\
    --input=$res_vx\
    --output=$bvx/wav2vec/$extraction_level/layer_$num_layer/$splitdirectory\
    --extraction_level=$extraction_level\
    --do_normalize=$normalize\
    --num_layer=$num_layer
  #sid/compute_vad_decision.sh --nj 40 --cmd "$train_cmd" $res_vx
  utils/fix_data_dir.sh $res_vx

done
#!/usr/bin/env bash
#!/bin/bash
#SBATCH --job-name=embedw2v
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=2
#SBATCH --partition=gpu
#SBATCH --gpus-per-node=1
#SBATCH --constraint='GPURAM_Max_16GB'
#SBATCH --mem=32G
#SBATCH --time=7-0:00:00

. ./cmd.sh
. ./path.sh

for ARGUMENT in "$@"
do
   KEY=$(echo $ARGUMENT | cut -f1 -d=)

   KEY_LENGTH=${#KEY}
   VALUE="${ARGUMENT:$KEY_LENGTH+1}"

   export "$KEY"="$VALUE"
done

normalize=$([[ -z  $normalize ]] && echo "True" || echo $normalize )
num_layer=$([[ -z  $num_layer ]] && echo "7" || echo  $num_layer  )
extraction_level=$([[ -z  $extraction_level ]] && echo "CNN" || echo $extraction_level )
pathartifact=$([ "$normalize" == True ] && echo "" ||  echo "not_normalized")


mfccdir=`pwd`/mfcc
audio=/data/coros1/qraymondaud/UrbanSound8K/audio
kaldi=/data/coros1/qraymondaud/UrbanSound8K/kaldi
w2v=/data/coros1/qraymondaud/UrbanSound8K/wav2vec/$pathartifact

echo $fold
if [[ -z $fold ]]; then
  echo "Specifiy an UrbanSound8K Fold to process"
  exit 1
fi

rm -rf $w2v/$extraction_level/layer_$num_layer/$fold
mkdir -p $w2v/$extraction_level/layer_$num_layer/$fold

#rm -rf $kaldi/$fold/
#mkdir -p $kaldi/$fold/

touch $w2v/$extraction_level/$num_layer/$fold/wav.scp  
#touch $kaldi/$fold/wav.scp $kaldi/$fold/utt2spk $kaldi/$fold/spk2utt

#for file in `ls $audio/$fold/*.wav`
#do
#  fbasename=`basename $file .wav`
#  echo "${fbasename} sox ${audio}/$fold/${fbasename}.wav -r 16000 -c 1 -b 16 -t wav - |" >> $kaldi/$fold/wav.scp
#  echo "${fbasename} ${fbasename}" >> $kaldi/$fold/utt2spk
#  echo "${fbasename} ${fbasename}" >> $kaldi/$fold/spk2utt
#done

utils/fix_data_dir.sh $kaldi/$fold/
# steps/make_mfcc.sh --mfcc-config conf/mfcc_hires.conf --nj 20 --cmd "$train_cmd" $kaldi/$fold/

./compute-w2v2-features.sh\
  --input=$kaldi/$fold\
  --output=$w2v/$extraction_level/layer_$num_layer/$fold\
  --extraction_level=$extraction_level\
  --do_normalize=$normalize\
  --num_layer=$num_layer


#Example Usage : sbatch run_prepro_urbansound_10fold.sh extraction_level=CNN num_layer=1 fold=fold1
# Verif :  copy-feats scp:/data/coros1/qraymondaud/UrbanSound8K/kaldi/fold1/data/raw_mfcc_fold1.1.scp ark,t:- | more


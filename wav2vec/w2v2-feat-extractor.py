#!/usr/bin/env python3

import argparse
import sys
import os
import time
from loguru import logger

logger.remove()
logger.add(sys.stderr, level="INFO")

# TODO: le support pour le décodage online devrait se faire comment?

# NOTE: Floating-point data seems to compress poorly, but it is possible to gain a >2x compression ratio
#       through compression when the frame size workaround is in use.
#       Unfortunately Kaldi does not support random seeking inside of compressed archives which is a requirement here.
#       Hence, you may need to rely on transparent filesystem compression for this usecase.

parser = argparse.ArgumentParser(description=
    "Generates Wav2Vec 2.0 features from an input dataset by extracting embedding layers.\n"
    "Those features are intended to be used as inputs for a TDNN-F instead of MFCC data.\n"
    "feats.scp and feats.ark will be generated inside of the output directory, which should work as a drop-in hires MFCC data directory.\n"
    "The embedded layer being 512/768 neurons wide, TDNN inputs should be resized accordingly.\n"
    "However, generated data may be significantly heavier than equivalent MFCC features due to the amount of features there are (even up to 0.5KiB per second of speech).")

parser.add_argument("--do_normalize", help="Wav2Vec Input Normalization, True by default", type=bool, default=True)
parser.add_argument("--extraction_level", help="CNN or TRSFMR extraction", default="CNN")
parser.add_argument("--num_layer", help="CNN[1:7] or TRSFMR[1:12] layer number", type=int, default=7 )
parser.add_argument("--input", help="Kaldi input data directory (which should contain wav.scp, etc.)")
parser.add_argument("--output", help="Kaldi output data directory (which will contain feats.scp, etc.)")
parser.add_argument("--model", help="Wav2Vec 2.0 model to extract features from", default="facebook/wav2vec2-base-960h")
parser.add_argument("--view-output", help="Print some information about the inputs and extracted features using matplotlib. This is merely for testing and will exit after the first source file has been processed", action="store_true")
parser.add_argument("--model-cache-dir", help="HuggingFace Transformers cache model directory. If unspecified, defaults to $TRANSFORMERS_CACHE, i.e. ~/.cache/huggingface.")
parser.add_argument("--normalize-waveform", help="Normalize the waveform from its signed integer limits to -1..1", action="store_true", default=False)
parser.add_argument("--measure-time", help="Measure model evaluation time for every file", action="store_true")
parser.add_argument("--force", help="Override output .scp/.ark if it exists already", action="store_true")
parser.add_argument("--no-frame-size-workaround", help="When set, do NOT duplicate frames. This is done to match the amount of frames per second MFCCs use (10ms sliding frames, as opposed to 20ms for the Wav2Vec 2.0 model we use). Disabling this may require iVector and TDNN related modifications and retraining. With the workaround, the dataset size is doubled, which can be mitigated with filesystem compression.", action="store_true")
args = parser.parse_args()

outsize = 512 if args.extraction_level == "CNN" else 768 

# Wav2Vec2 Base Facebook : len(model.feature_extractor.conv_layers) == 7
args.num_layer = 7 if ( args.extraction_level == "CNN" and  args.num_layer > 7 ) or args.num_layer < 1 else args.num_layer  

# Wav2Vec2 Base Facebook : len(model.encoder.layers) == 12
args.num_layer = 12 if ( args.extraction_level == "TRSFMR" and args.num_layer > 12 ) or args.num_layer < 1 else args.num_layer  


if os.path.isfile(f"{args.output}/feats.ark") or os.path.isfile(f"{args.output}/feats.scp"):
    if args.force:
        logger.warning(f"Features were already output to {args.output}, but --force was specified; continuing")
    else:
        logger.error(f"Features were already output to {args.output}, refusing to proceed. Specify --force to continue anyway.")
        exit(1)

if args.model_cache_dir is not None:
    os.environ['TRANSFORMERS_CACHE'] = args.model_cache_dir

# better import heavy libraries late so we don't have to wait for ages when using --help
logger.info("Importing libraries")

# general IO stuff
from kaldiio import WriteHelper, ReadHelper, load_scp_sequential
import numpy as np

# wav2vec support
import torch
from transformers import Wav2Vec2Model, Wav2Vec2FeatureExtractor

# visualization
from matplotlib import pyplot as plt

logger.info(f"Initializing Wav2Vec 2.0 feature extractor from pre-trained model {args.model}")
extractor = Wav2Vec2FeatureExtractor.from_pretrained(args.model)

logger.info(f"Initializing Wav2Vec 2.0 model from pre-trained model {args.model}")
model = Wav2Vec2Model.from_pretrained(args.model)

device = "cpu"
if torch.cuda.is_available():
    logger.info(f"Migrating model to CUDA")
    device = "cuda"
    model.to(device)
    logger.info(f"Running on {torch.cuda.get_device_name(0)}")
else:
    logger.warning("CUDA unavailable. Inference will be performed on CPU, which is significantly slower.")

def extract_features(waveform):
    with torch.no_grad():
        logger.debug("Preparing inputs")

        # using return_tensors="pt" seems to hit a slow code path in Transformers/PyTorch for now...
        inputs = extractor(waveform, sampling_rate=16_000, return_tensors="np", padding=True, do_normalize=args.do_normalize)
        # ... so get a numpy output and convert to a tensor manually
        inputs = torch.from_numpy(inputs.input_values).to(device)

        logger.debug("Processing model")

        if (args.extraction_level == "CNN"):
            model.feature_extractor.conv_layers = model.feature_extractor.conv_layers[:args.num_layer]
            features_layer = model.feature_extractor(inputs)
            features_layer = features_layer.transpose(1, 2)
            features_layer = features_layer[0].cpu()

        elif (args.extraction_level == "TRSFMR"):
            model.encoder.layers = model.encoder.layers[:args.num_layer]
            features_layer = model(inputs)
            features_layer = features_layer.last_hidden_state
            features_layer = features_layer[0].cpu()

        logger.info(f" Input Shape: {inputs.shape} - Feature Shape: {features_layer.shape}")

        # print(len(outputs["hidden_states"]))
    
    # assert(len(features_layer[0]) == outsize)

    if args.view_output:
        states = features_layer
        print("Done. Doing visualization stuff now.", file=sys.stderr)
        print(f"Summary of inputs: {inputs}", file=sys.stderr)
        print(f"Summary of model output: {outputs}", file=sys.stderr)

        print(f"Model output has {len(states[0])} features and {len(states)} collections of states")

        print("Generating plot for input")
        plt.plot(waveform)
        #plt.plot(inputs.input_values)
        #plt.plot(states.T[:64].T)
        plt.tight_layout()
        plt.savefig("waveform.png", dpi=300)
            
        print("Generating heatmap for extracted features")
        trans_feats = states.T
        #for i in range(len(trans_feats)):
        #    trans_feats[i] = (trans_feats[i] - np.mean(trans_feats[i])) / np.linalg.norm(trans_feats[i])
        plt.imshow(trans_feats, interpolation="none", cmap="hot")
        #plt.plot(states.T[:64].T)
        plt.tight_layout()
        plt.savefig("features-heatmap.png", dpi=300)

        exit(0)

    return features_layer

logger.info(f"Creating output directory {args.output}")
os.makedirs(args.output, exist_ok=True)

logger.info(f"Importing Kaldi dataset from {args.input}")
reader = load_scp_sequential(f"{args.input}/wav.scp")
if True:
#with ReadHelper(f'ark:extract-segments scp:{args.input}/wav.scp {args.input}/segments ark:-|') as reader:
    with WriteHelper(f'ark,scp:{args.output}/feats.ark,{args.output}/feats.scp') as writer:
        last_load = time.time()
        for key, (sf, wav) in reader:
            start = time.time()

            wav = wav.astype(np.float32)
            sample_duration = len(wav) / 16000.0

            if args.normalize_waveform:
                wav = np.divide(wav, 32768)

            logger.info(f"Processing {key} ({len(wav)} samples = {sample_duration}s, disk load = {start - last_load}s)")

            features = np.empty((0,outsize))
            
            chunk_secs = 30 #avoid OOM
            samples_per_chunk = int(16000 * 0.02)
            for i in range(0, len(wav), 16000*chunk_secs):
                    #logger.info(f"test {i}")
                    sample = wav[i:min(len(wav),i+16000*chunk_secs)]

                    if len(sample) >= samples_per_chunk * 2: # rough estimate to avoid convnet errors
                        outputs = extract_features(sample).detach().numpy()
                    else:
                        # This is required because too few samples makes the decoder derp out
                        logger.info(f"Skipped last sample of size {len(sample)}")

                        outputs = np.zeros((len(sample) // samples_per_chunk, outsize))
                        
                    #assert(outputs.shape[0] == 1)
                    features = np.concatenate((features, outputs))

            # features = extract_features(wav).detach().numpy()

            assert(features.shape[1] == outsize)

            # if not args.no_frame_size_workaround:
            #     # Repeating on axis 0 makes it so that every line (i.e. set of features) is duplicated.
            #     # Meaning that will we get the same column count and twice the row count.
            #     features = np.repeat(features, 2, axis=0)

            save_begin = time.time()
            writer(key, features)

            end = time.time()

            if args.measure_time:
                logger.info(f"{sample_duration / (end - start)} x realtime (disk write = {end - save_begin}s)")
            
            last_load = end
